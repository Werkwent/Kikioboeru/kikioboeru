class TokenRegistration {
  tokenName;
  assertion;
  challenge;
  gql;

  async getChallenge(endpoint = '/graphql') {
    // Initialize a new GraphQL query
    const gql = new GraphQL(endpoint);

    // Create our query
    gql.setQuery(`query($tokenName: String) {
      webauthn {
        register(tokenName: $tokenName) {
          rp {
            name
            id
          }
          pubKeyCredParams {
            type
            alg
          }
          challenge
          attestation
          user {
            name
            id
            displayName
            avatar
          }
          authenticatorSelection {
            requireResidentKey
            userVerification
          }
          timeout
        }
      }
    }`);

    // Add our variables
    gql.addVariable('tokenName', this.tokenName);
    
    // Send our query to the server
    // Await the results
    const res = await gql.execute();

    // Get our challenge
    this.challenge = res.data.webauthn.register;
  }

  async assert() {
    const options = {
      challenge: Uint8Array.from(this.base64UrlDecode(this.challenge.challenge), c => c.charCodeAt(0)),
      rp: this.challenge.rp,
      user: {
        ...this.challenge.user,
        id: Uint8Array.from(
          window.atob(this.challenge.user.id),
          c => c.charCodeAt(0)
        )
      },
      pubKeyCredParams: this.challenge.pubKeyCredParams,
      timeout: this.challenge.timeout
    };

    this.assertion = await navigator.credentials.create({
      publicKey: options
    });
  }

  async submit(endpoint = '/graphql') {
    // Build our credential
    const publicKeyCredential = {
      id: this.assertion.id,
      type: this.assertion.type,
      rawId: this.arrayToBase64String(new Uint8Array(this.assertion.rawId)),
      response: {
        clientDataJSON: this.arrayToBase64String(
          new Uint8Array(this.assertion.response.clientDataJSON)
        ),
      },
    };
    
    if (this.assertion.response.attestationObject !== undefined) {
      publicKeyCredential.response.attestationObject = this.arrayToBase64String(
          new Uint8Array(this.assertion.response.attestationObject)
      );
    }

    if (this.assertion.response.authenticatorData !== undefined) {
      publicKeyCredential.response.authenticatorData = this.arrayToBase64String(
          new Uint8Array(this.assertion.response.authenticatorData)
      );
    }
  
    if (this.assertion.response.signature !== undefined) {
      publicKeyCredential.response.signature = this.arrayToBase64String(
          new Uint8Array(this.assertion.response.signature)
      );
    }
  
    if (this.assertion.response.userHandle !== undefined) {
      publicKeyCredential.response.userHandle = this.arrayToBase64String(
          new Uint8Array(this.assertion.response.userHandle)
      );
    }

    // Initialize a new GraphQL query
    const gql = new GraphQL(endpoint);

    // Build our mutation
    gql.setQuery(`mutation($publicKeyCredential: PublicKeyCredential!) {
      registerToken(publicKeyCredential: $publicKeyCredential) {
        success
        message
      }
    }`);

    // Add our assertion to the variables
    gql.addVariable('publicKeyCredential', publicKeyCredential);

    // Submit out assertion
    const resp = await gql.execute();

    // Return our response
    return resp;
  }

  async register(endpoint = '/graphql', tokenName = '') {
    // Set our token name
    this.tokenName = tokenName;

    // Request a challenge
    await this.getChallenge(endpoint);

    // Assert our challenge
    await this.assert();
    console.log(this.assertion);

    // Submit our assertion
    const result = await this.submit(endpoint);

    // Return our result
    return result;
  }

  base64UrlDecode(input) {
    input = input
        .replace(/-/g, '+')
        .replace(/_/g, '/');
  
    const pad = input.length % 4;
    if (pad) {
      if (pad === 1) {
        throw new Error('InvalidLengthError: Input base64url string is the wrong length to determine padding');
      }
      input += new Array(5-pad).join('=');
    }
  
    return window.atob(input);
  };

  arrayToBase64String(a) {
    return window.btoa(String.fromCharCode(...a));
  }

}