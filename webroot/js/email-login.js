class EmailLogin {
  async getToken(endpoint = '/graphql', username) {
    // Initialize a new GraphQL query
    const gql = new GraphQL(endpoint);

    // Create our query
    gql.setQuery(`query($username: String!) {
      emailLogin(username: $username) {
        success
        message
      }
    }`);

    // Add our username
    gql.addVariable('username', username);

    // Send our query to the server
    // Await the results
    return await gql.execute();
  }

  async login(endpoint = '/graphql', username, code) {
    // Initialize a new GraphQL query
    const gql = new GraphQL(endpoint);

    // Create our query
    gql.setQuery(`mutation($username: String!, $code: String!) {
      emailLogin(username: $username, code: $code) {
        success
        message
      }
    }`);

    // Add our variables
    gql.addVariable('username', username);
    gql.addVariable('code', code);

    // Send our query to the server
    // Await the results
    return await gql.execute();
  }
}