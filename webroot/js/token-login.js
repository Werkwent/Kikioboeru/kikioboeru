class TokenLogin {
  username;
  challenge;
  assertion;

  async getChallenge(endpoint = '/graphql') {
    // Initialize a new GraphQL query
    const gql = new GraphQL(endpoint);

    // Create our query
    gql.setQuery(`query($username: String!) {
      webauthn {
        login(username: $username) {
          rpId
          allowCredentials {
            type
            id
          }
          challenge
          userVerification
          timeout
        }
      }
    }`);

    // Add our username
    gql.addVariable('username', this.username);

    // Send our query to the server
    // Await the results
    const res = await gql.execute();

    // Get our challenge
    this.challenge = res.data.webauthn.login;
  }

  async assert() {
    const options = {
      challenge: Uint8Array.from(this.base64UrlDecode(this.challenge.challenge), c => c.charCodeAt(0)),
      rpId: this.challenge.rpId,
      allowCredentials: this.challenge.allowCredentials.map(allowCredential => ({
        ...allowCredential,
        id: Uint8Array.from(
          this.base64UrlDecode(allowCredential.id),
          c => c.charCodeAt(0)
        )
      })),
      timeout: this.challenge.timeout
    };

    this.assertion = await navigator.credentials.get({
      publicKey: options
    });
  }

  async submit(endpoint = '/graphql') {
    // Build our credential
    const publicKeyCredential = {
      id: this.assertion.id,
      type: this.assertion.type,
      rawId: this.arrayToBase64String(new Uint8Array(this.assertion.rawId)),
      response: {
        clientDataJSON: this.arrayToBase64String(
          new Uint8Array(this.assertion.response.clientDataJSON)
        ),
      },
    };
    
    if (this.assertion.response.attestationObject !== undefined) {
      publicKeyCredential.response.attestationObject = this.arrayToBase64String(
          new Uint8Array(this.assertion.response.attestationObject)
      );
    }

    if (this.assertion.response.authenticatorData !== undefined) {
      publicKeyCredential.response.authenticatorData = this.arrayToBase64String(
          new Uint8Array(this.assertion.response.authenticatorData)
      );
    }
  
    if (this.assertion.response.signature !== undefined) {
      publicKeyCredential.response.signature = this.arrayToBase64String(
          new Uint8Array(this.assertion.response.signature)
      );
    }
  
    if (this.assertion.response.userHandle !== undefined) {
      publicKeyCredential.response.userHandle = this.arrayToBase64String(
          new Uint8Array(this.assertion.response.userHandle)
      );
    }

    // Initialize a new GraphQL query
    const gql = new GraphQL(endpoint);

    // Set out query
    gql.setQuery(`mutation($publicKeyCredential: PublicKeyCredential!) {
      loginToken(publicKeyCredential: $publicKeyCredential) {
        success
        message
      }
    }`);

    // Add our assertion to the variables
    gql.addVariable('publicKeyCredential', publicKeyCredential);

    // Submit out assertion
    const resp = await gql.execute();

    // Return our response
    return resp;
  }

  async login(endpoint = '/graphql', username) {
    // Set our username
    this.username = username;

    // Request a challenge
    await this.getChallenge(endpoint);

    // Assert our challenge
    await this.assert();

    // Submit our challenge
    const resp = await this.submit(endpoint);
    
    // Return our response
    return resp;
  }

  base64UrlDecode(input) {
    input = input
        .replace(/-/g, '+')
        .replace(/_/g, '/');
  
    const pad = input.length % 4;
    if (pad) {
      if (pad === 1) {
        throw new Error('InvalidLengthError: Input base64url string is the wrong length to determine padding');
      }
      input += new Array(5-pad).join('=');
    }
  
    return window.atob(input);
  };

  arrayToBase64String(a) {
    return window.btoa(String.fromCharCode(...a));
  }
}