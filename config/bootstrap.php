<?php
use Cake\Core\Configure;
use Cake\Controller\Controller;
use Admiral\Admiral\Menu;
use Admiral\Admiral\Settings;
use Admiral\Admiral\Permission;
use Admiral\Admiral\Asset;
use Admiral\GraphQL\Types;
use Admiral\GraphQL\Types\Definition\QueryType;
use Admiral\GraphQL\Types\Definition\MutationType;

if(PHP_SAPI === 'fpm-fcgi'){
  $controller = new Controller();

  $controller->loadComponent('Admiral/Admiral.Dashboard');

  Configure::write('Admiral\Admiral.pluginOptions',[]);
  Configure::write('Admiral\Admiral.menuItems',[]);
  Configure::write('Admiral\Admiral.dashboardCells',[]);
  Configure::write('Admiral\Admiral.permissionNodes',[]);

  Menu::add('main_menu', [
    'name'=>'dashboard',
    'label'=>'Dashboard',
    'icon'=>'fas fa-tachometer-alt',
    'url' => [
      "plugin"=>"Admiral/Admiral",
      "controller" => "Admin",
      "action"=>"index"
    ],
    'weight' => 0
  ]);

  Menu::add('main_menu', [
    'name'=>'media',
    'label'=>'Media',
    'icon'=>'fa fa-camera',
    'children' => [
      [
        'label' => 'Library',
        'url' => [
          "plugin"=>"Admiral/Admiral",
          "controller" => "Media",
          "action"=>"index"
        ],
        'permissions' => [
          'admiral.admiral.cms.access',
          'admiral.admiral.media.view'
        ]
      ],
      [
        'label' => 'Add Media',
        'url' => [
          "plugin"=>"Admiral/Admiral",
          "controller" => "Media",
          "action"=>"add"
        ],
        'permissions' => [
          'admiral.admiral.cms.access',
          'admiral.admiral.media.add'
        ]
      ]
    ],
    'weight' => 1
  ]);

  Menu::add('main_menu', [
    'name' => 'users',
    'label' => 'Users',
    'icon' => 'fa fa-user',
    'children' => [
      [
        'label' => 'All Users',
        'url' => [
          "plugin"=>'Admiral/Admiral',
          "controller" => "Users",
          "action"=>"index"
        ]
      ],
      [
        'label' => 'Add User',
        'url' => '#'
      ],
      [
        'label' => 'My Profile',
        'url' => [
          "plugin"=>'Admiral/Admiral',
          "controller" => "Users",
          "action"=>"profile"
        ]
      ]
    ],
    'weight' => 1
  ]);

  Menu::add('settings', [
    'name' => 'general',
    'label' => 'General',
    'icon' => 'fas fa-cogs',
    'url' => [
      "plugin"=>'Admiral/Admiral',
      'controller' => 'Settings',
      'action' => 'index'
    ],
    'permissions' => [
      'admiral.admiral.cms.access',
      'admiral.admiral.settings.edit'
    ],
    'weight' => 10
  ]);

  Settings::add([
    'label' => 'General Settings',
    'name' => 'general-settings',
    'options' => []
  ]);

  // Add our dashboard cells
  $controller->Dashboard
  ->registerCell([
    'title' =>'Registered Users',
    'ns' => 'Admiral/Admiral',
    'cell' => 'UsersCount'
  ]);

  // Register assets
  Asset::script('Admiral/Admiral.jquery-3.3.1.min');
  Asset::script('Admiral/Admiral.popper.min');
  Asset::script('Admiral/Admiral.bootstrap.min');
  Asset::script('Admiral/Admiral.handlebars.min');
  Asset::script('Admiral/Admiral.sha256.min');
  Asset::script('Admiral/Admiral.tinymce/tinymce.min');
  Asset::script('Admiral/Admiral.toastr.min');
  Asset::script('Admiral/Admiral.chart.min');
  Asset::script('Admiral/Admiral.bootstrap4-toggle.min');
  Asset::script('Admiral/Admiral.token-registration');
  Asset::script('Admiral/Admiral.token-login');
  Asset::script('Admiral/Admiral.email-login');
  Asset::script('Admiral/Admiral.index');
  Asset::script('Admiral/GraphQL.graphql');
  Asset::script('FinlayDaG33k/FileUpload.resumable');
  Asset::script('FinlayDaG33k/FileUpload.resumable-uploader');

  // Register GraphQL types
  Types::register('User', (new \Admiral\Admiral\GraphQL\Type\Definition\UserType())->config());
  Types::register('Webauthn', (new \Admiral\Admiral\GraphQL\Type\Definition\WebauthnType())->config());
  Types::register('TokenRegisterChallenge', (new \Admiral\Admiral\GraphQL\Type\Definition\TokenRegisterChallengeType)->config());
  Types::register('TokenLoginChallenge', (new \Admiral\Admiral\GraphQL\Type\Definition\TokenLoginChallengeType)->config());
  Types::registerInput('PublicKeyCredential', (new \Admiral\Admiral\GraphQL\Type\Definition\PublicKeyCredentialType())->config());

  // Register GraphQL fields for Queries
  QueryType::addField('user', [
    'type' => Types::get('User'),
    'description' => 'Returns a specific user',
  ]);
  QueryType::addField('webauthn', [
    'type' => Types::get('Webauthn'),
    'description' => 'Interact with the Webauthn capability of Admiral',
    'resolve' => function($rootValue, $args) {
      return [];
    }
  ]);
  QueryType::addField('emailLogin', [
    'type' => Types::get('Status'),
    'description' => 'Request a login token by email of username',
    'args' => [
      'username' => Types::get('string'),
    ],
    'resolve' => function($rootValue, $args, $context, $info) {
      return (new \Admiral\Admiral\GraphQL\Resolver\EmailLoginResolver)->getToken($rootValue, $args, $context, $info);
    }
  ]);

  // Register GraphQL fields for Mutations
  MutationType::addField('registerToken',[
    'type' => Types::get('Status'),
    'description' => 'Register a WebAuthn token after resolving the challenge',
    'args' => [
      'publicKeyCredential' => Types::input('PublicKeyCredential'),
    ],
    'resolve' => function($rootValue, $args, $context, $info) {
      return (new \Admiral\Admiral\GraphQL\Resolver\RegisterTokenResolver)->resolver($rootValue, $args, $context, $info);
    }
  ]);
  MutationType::addField('loginToken',[
    'type' => Types::get('Status'),
    'description' => 'Login a WebAuthn token after resolving the challenge',
    'args' => [
      'publicKeyCredential' => Types::input('PublicKeyCredential'),
    ],
    'resolve' => function($rootValue, $args, $context, $info) {
      return (new \Admiral\Admiral\GraphQL\Resolver\LoginTokenResolver)->resolver($rootValue, $args, $context, $info);
    }
  ]);
  MutationType::addField('emailLogin', [
    'type' => Types::get('Status'),
    'description' => 'Submit login code to login a user',
    'args' => [
      'username' => Types::get('string'),
      'code' => Types::get('string'),
    ],
    'resolve' => function($rootValue, $args, $context, $info) {
      return (new \Admiral\Admiral\GraphQL\Resolver\EmailLoginResolver)->login($rootValue, $args, $context, $info);
    }
  ]);
  MutationType::addField('updateAvatar', [
    'type' => Types::get('Status'),
    'description' => 'Update the avatar for the currently logged in user',
    'args' => [
      'data' => Types::input('string'),
    ],
    'resolve' => function($rootValue, $args, $context, $info) {
      return (new \Admiral\Admiral\GraphQL\Resolver\UserResolver)->updateAvatar($rootValue, $args, $context, $info);
    }
  ]);
}
