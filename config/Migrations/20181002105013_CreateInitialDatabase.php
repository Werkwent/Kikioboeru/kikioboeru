<?php
use Migrations\AbstractMigration;

class CreateInitialDatabase extends AbstractMigration {
  /**
   * Change Method.
   *
   * More information on this method is available here:
   * http://docs.phinx.org/en/latest/migrations.html#the-change-method
   * @return void
   */
  public function change() {
    // Create the users table
    $table = $this->table('users')
                  ->addColumn('username','string',['default' => null,'limit' => 255,'null' => false])
                  ->addColumn('email','string',['default' => null,'null' => false])
                  ->addColumn('password','string',['default' => null,'null' => true])
                  ->addColumn('created','datetime',['default' => null,'null' => false])
                  ->save();

    // Create the users_details table
    $table = $this->table('users_details')
                  ->addColumn('firstname','string',['default' => null,'limit' => 255,'null' => false])
                  ->addColumn('lastname','string',['default' => null,'limit' => 255,'null' => false])
                  ->addForeignKey('id','users','id', ['delete'=> 'CASCADE', 'update'=> 'NO_ACTION'])
                  ->save();

    // Create the roles table
    $table = $this->table('roles')
                  ->addColumn('name','string',['default' => null,'null' => false])
                  ->save();

    // Create the user_roles table
    $table = $this->table('users_roles',['id' => false,'primary_key' => ['user_id','role_id']])
                  ->addColumn('user_id','integer',['default' => null,'limit' => 11,'null' => false])
                  ->addColumn('role_id','integer',['default' => null,'limit' => 11,'null' => false])
                  ->addForeignKey('user_id','users','id', ['delete'=> 'CASCADE', 'update'=> 'NO_ACTION'])
                  ->addForeignKey('role_id','roles','id', ['delete'=> 'CASCADE', 'update'=> 'NO_ACTION'])
                  ->save();

    // Create the permissions table
    $table = $this->table('permissions')
                  ->addColumn('name','string',['default' => false,'null' => false])
                  ->save();

    // Create the table to link roles to the permissions
    $table = $this->table('roles_permissions', ['id' => false, 'primary_key' => ['role_id', 'permission_id']])
                  ->addColumn('role_id','integer',['default' => null,'limit' => 11,'null' => false])
                  ->addColumn('permission_id','integer',['default' => null,'limit' => 11,'null' => false])
                  ->addForeignKey('role_id','roles','id', ['delete'=> 'CASCADE', 'update'=> 'NO_ACTION'])
                  ->addForeignKey('permission_id','permissions','id', ['delete'=> 'CASCADE', 'update'=> 'NO_ACTION'])
                  ->save();

    // Create the options table
    $table = $this->table('options')
                  ->addColumn('name','string',['default' => null,'limit' => 255,'null' => false])
                  ->addColumn('value','text',['null' => true])
                  ->save();
  }
}
