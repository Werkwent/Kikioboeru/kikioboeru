<?php
use Migrations\AbstractMigration;

class AddResettokensTable extends AbstractMigration {
  /**
   * Change Method.
   *
   * More information on this method is available here:
   * http://docs.phinx.org/en/latest/migrations.html#the-change-method
   * @return void
   */
    public function change() {
        $this->table('reset_tokens')
             ->addColumn('user_id','integer',['default' => null,'limit' => 11,'null' => false])
             ->addColumn('token','string',['default' => null,'limit' => 255,'null' => true])
             ->addColumn('created','datetime',['default' => null,'null' => false])
             ->addForeignKey('user_id','users','id', ['delete'=> 'CASCADE', 'update'=> 'NO_ACTION'])
             ->save();
    }
}
