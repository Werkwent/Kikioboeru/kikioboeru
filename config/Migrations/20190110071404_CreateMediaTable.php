<?php
use Migrations\AbstractMigration;

class CreateMediaTable extends AbstractMigration {
  /**
   * Change Method.
   *
   * More information on this method is available here:
   * http://docs.phinx.org/en/latest/migrations.html#the-change-method
   * @return void
   */
  public function change() {
    $table = $this->table('media')
      ->addColumn('filename','string',['default' => null,'limit' => 255,'null' => false])
      ->addColumn('path','string',['default' => null,'null' => false])
      ->addColumn('created','datetime',['default' => null,'null' => false])
      ->addColumn('modified','datetime',['default' => null,'null' => false])
      ->addColumn('type','string',['default' => null,'limit' => 255,'null' => false])
      ->addColumn('size','biginteger',['default' => null,'limit' => 255,'null' => false])
      ->save();
  }
}
