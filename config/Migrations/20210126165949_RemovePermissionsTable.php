<?php
use Migrations\AbstractMigration;

class RemovePermissionsTable extends AbstractMigration {
  public function up() {
    $this->table('roles_permissions')
      ->drop()
      ->save();

    $this->table('permissions')
      ->drop()
      ->save();
  }
}
