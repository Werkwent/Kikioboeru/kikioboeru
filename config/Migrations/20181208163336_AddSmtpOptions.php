<?php
use Migrations\AbstractMigration;
use Cake\ORM\TableRegistry;

class AddSmtpOptions extends AbstractMigration {
  /**
   * Change Method.
   *
   * More information on this method is available here:
   * http://docs.phinx.org/en/latest/migrations.html#the-change-method
   * @return void
   */
  public function change() {
    $optionsTable = TableRegistry::get('Options');

    // Build options for the SMTP settings
    $data = [
      'smtp_host' => 'localhost',
      'smtp_port' => 25,
      'smtp_username' => '',
      'smtp_password' => '',
      'smtp_usetls' => true
    ];
    
    foreach($data as $name => $value){
      $this->table('options')->insert(['name' => $name, 'value' => $value])->saveData();
    }
  }
}
