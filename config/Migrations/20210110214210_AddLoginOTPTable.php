<?php
use Migrations\AbstractMigration;

class AddLoginOTPTable extends AbstractMigration {
  public function change() {
    $table = $this->table('login_otp')
      ->addColumn('user_id', 'integer', ['null' => false,'signed' => true])                 
      ->addColumn('code', 'string', ['null' => false])
      ->addColumn('created', 'datetime', ['null' => false,])
      ->addIndex(['user_id'], ['unique' => false, 'name' => 'id_auth_tokens_user_id'])
      ->addForeignKey('user_id','users','id', ['delete'=> 'CASCADE', 'update'=> 'CASCADE'])
      ->Save();
  }
}
