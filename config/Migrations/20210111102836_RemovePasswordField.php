<?php
use Migrations\AbstractMigration;
use Cake\ORM\TableRegistry;

class RemovePasswordField extends AbstractMigration {
  public function change() {
     $this->table('users')
      ->removeColumn('password')
      ->save();
  }
}
