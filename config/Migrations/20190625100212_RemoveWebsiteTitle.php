<?php
use Migrations\AbstractMigration;
use Cake\ORM\TableRegistry;

class RemoveWebsiteTitle extends AbstractMigration {
 /**
  * Change Method.
  *
  * More information on this method is available here:
  * http://docs.phinx.org/en/latest/migrations.html#the-change-method
  * @return void
  */
  public function change() {
    $this->execute("DELETE FROM `options` WHERE 'name'='site_title'");
  }
}