<?php
use Migrations\AbstractMigration;
use Cake\ORM\TableRegistry;

class AddSiteTitle extends AbstractMigration {
  /**
   * Change Method.
   *
   * More information on this method is available here:
   * http://docs.phinx.org/en/latest/migrations.html#the-change-method
   * @return void
   */
  public function change() {
    // Insert site title option
    $this->table('options')->insert(['name' => 'site_title', 'value' => 'My Website'])->saveData();
  }
}
