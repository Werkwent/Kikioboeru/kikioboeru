<?php
  namespace Admiral\Admiral\Webauthn;

  use Cose\Algorithm\ManagerFactory;
  use Cose\Algorithm\Signature\RSA;
  use Cose\Algorithm\Signature\ECDSA;
  use Cose\Algorithm\Signature\EdDSA;

  class AlgorithmFactory {
    private $managerFactory;

    public function __construct() {
      // Initialize our factory
      $this->managerFactory = new ManagerFactory();

      // Add our algorithms
      $this->addAlgorithms();
    }

    private function addAlgorithms() {
      $this->managerFactory->add('RS1', new RSA\RS1());
      $this->managerFactory->add('RS256', new RSA\RS256());
      $this->managerFactory->add('RS384', new RSA\RS384());
      $this->managerFactory->add('RS512', new RSA\RS512());
      $this->managerFactory->add('PS256', new RSA\PS256());
      $this->managerFactory->add('PS384', new RSA\PS384());
      $this->managerFactory->add('PS512', new RSA\PS512());
      $this->managerFactory->add('ES256', new ECDSA\ES256());
      $this->managerFactory->add('ES256K', new ECDSA\ES256K());
      $this->managerFactory->add('ES384', new ECDSA\ES384());
      $this->managerFactory->add('ES512', new ECDSA\ES512());
      $this->managerFactory->add('Ed25519', new EdDSA\Ed25519());
    }

    public function createManager(array $algorithms) {
      return $this->managerFactory->create($algorithms);
    }
  }