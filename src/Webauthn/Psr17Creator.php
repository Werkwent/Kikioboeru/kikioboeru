<?php
  namespace Admiral\Admiral\Webauthn;

  use Nyholm\Psr7\Factory\Psr17Factory;
  use Nyholm\Psr7Server\ServerRequestCreator;

  class Psr17Creator {
    private $psr17Factory;

    public function __construct() {
      $this->psr17Factory = new Psr17Factory();
    }

    public function createServerRequest() {
      return new ServerRequestCreator(
        $this->psr17Factory, // ServerRequestFactory
        $this->psr17Factory, // UriFactory
        $this->psr17Factory, // UploadedFileFactory
        $this->psr17Factory  // StreamFactory
      );
    }
  }