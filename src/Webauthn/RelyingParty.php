<?php
  namespace Admiral\Admiral\Webauthn;

  use Cake\Core\Configure;
  use Webauthn\PublicKeyCredentialRpEntity;

  class RelyingParty {
    private $partyName;
    private $httpHost;
    private $icon;

    public function __construct() {
      // Set our party name
      $this->partyName = env('ADMIRAL_BRANDING_NAME', 'Admiral');

      // Set our HTTP Host
      $this->httpHost = $_SERVER['HTTP_HOST'];

      // Set our icon to null
      // TODO: Favicon?
      $this->icon = null;
    }

    public function getParty() {
      return new PublicKeyCredentialRpEntity(
        $this->partyName,
        $this->httpHost,
        $this->icon
      );
    }
  }