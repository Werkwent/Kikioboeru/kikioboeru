<?php
  namespace Admiral\Admiral\Webauthn;

  use Webauthn\PublicKeyCredentialUserEntity;

  class UserEntity {
    private $id;
    private $email;
    private $username;
    private $avatar;

    public function __construct($user) {
      // Set our ID
      $this->id = $user->id;

      // Set our Email
      $this->email = $user->email;

      // Set our Username
      $this->username = $user->username;

      // Set our avatar to null
      // TODO: Use actual avatar
      $this->avatar = null;
    }

    public function getUser() {
      return new PublicKeyCredentialUserEntity(
        $this->email,
        $this->id,
        $this->username,
        $this->avatar
      );
    }
  }