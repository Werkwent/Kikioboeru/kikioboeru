<?php
  namespace Admiral\Admiral\Webauthn\Repository;

  use Cake\ORM\TableRegistry;
  use Cake\I18n\Time;
  use Webauthn\PublicKeyCredentialSource;
  use Webauthn\PublicKeyCredentialSourceRepository as PublicKeyCredentialSourceRepositoryInterface;
  use Webauthn\PublicKeyCredentialUserEntity;

  class DatabaseRepository implements PublicKeyCredentialSourceRepositoryInterface {
    private $tokensTable;

    public function __construct() {
      // Get our model
      $this->tokensTable = TableRegistry::getTableLocator()->get('Admiral/Admiral.Webauthntokens');
    }

    /**
     * TODO: Further documentation
     * 
     * @param string $publicKeyCredentialId
     * @return \Webauthn\PublicKeyCredentialSource|null
     */
    public function findOneByCredentialId(string $publicKeyCredentialId): ?PublicKeyCredentialSource {
      // Base64 encode our credentialId
      $credentialId = base64_encode($publicKeyCredentialId);

      // Find our token
      $token = $this->tokensTable->findByCredentialId($credentialId)->first();

      // Check if a token is found
      // If not, return null
      if(!$token) return null;

      // Token was found
      // Get the data and decode it
      $data = json_decode($token->data, true);

      // Return our credential
      return PublicKeyCredentialSource::createFromArray($data);
    }

    /**
     * TODO: Further documentation
     * 
     * @param \Webauthn\PublicKeyCredentialUserEntity $publicKeyCredentialUserEntity
     * @return PublicKeyCredentialSource[]
     */
    public function findAllForUserEntity(PublicKeyCredentialUserEntity $publicKeyCredentialUserEntity): array {
      // Create an array to hold our credentials
      $credentials = [];

      // Find all tokens for a user
      $tokens = $this->tokensTable->findByUserId($publicKeyCredentialUserEntity->getId())->all();
      
      // Loop over each token
      foreach($tokens as $token) {
        // Json decode our token data
        $tokenData = json_decode($token->data, true);

        // Add our token to the array
        $credentials[$token->tokenname] = PublicKeyCredentialSource::createFromArray($tokenData);
      }

      // Return our credentials
      return $credentials;
    }

    /**
     * TODO: Further documentation
     * 
     * @param \Webauthn\PublicKeyCredentialSource $publicKeyCredentialSource
     * @return void
     */
    public function saveCredentialSource(PublicKeyCredentialSource $publicKeyCredentialSource, string $tokenName = ''): void {
      // Base64 our credential id
      $credentialId = base64_encode($publicKeyCredentialSource->getPublicKeyCredentialId());

      // Check if this token already exists
      // If so, update entity
      // If no, create a new one
      $entity = $this->tokensTable->findByCredentialId($credentialId)->first();
      if(!$entity) {
        // Set default entity values
        $entity = $this->tokensTable->newEntity();
        $entity->user_id = $publicKeyCredentialSource->getUserHandle();
        $entity->tokenname = $tokenName;
        $entity->credential_id = $credentialId;
        $entity->created = new Time();
      }

      // Set out credential data
      $entity->data = json_encode($publicKeyCredentialSource);

      // Save our token
      $this->tokensTable->save($entity);

      return;
    }
  }