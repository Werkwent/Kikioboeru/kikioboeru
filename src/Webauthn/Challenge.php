<?php
  namespace Admiral\Admiral\Webauthn;

  use Cake\Utility\Security;

  class Challenge {
    private $randomBytes;

    public function __construct(int $challengeBytes = 16) {
      $this->randomBytes = Security::randomBytes($challengeBytes);
    }

    public function getChallengeBytes() {
      return $this->randomBytes;
    }

    public function getChallengeHex() {
      return \bin2hex($this->randomBytes);
    }
  }