<div class="table-responsive">
  <table role="presentation" class="table table-striped">
    <thead>
      <?= $this->Html->tableHeaders([
        [__d('Admiral/Admiral','Preview') => ['class' => 'col-1']], 
        [__d('Admiral/Admiral','Filename') => ['class' => 'col-6']], 
        [__d('Admiral/Admiral','Filesize') => ['class' => 'col']], 
        [__d('Admiral/Admiral','Actions') => ['class' => 'col-1']]
      ]); ?>
    </thead>
    <tbody class="files">
      <?php foreach($media as $file){ ?>
        <tr id="tr-<?= h($file->id); ?>">
          <td>
            <?php 
              switch($file->type){
                case "image":
                  ?>
                    <?= $this->Html->image($file->path, ['style'=> "max-height: 50px"]); ?>
                  <?php
                  break;
                default:
                  ?>
                    <i class="fas fa-file fa-3x"></i>
                  <?php
                  break;
              }
            ?>
          </td>
          <td><?= h($file->filename); ?></td>
          <td>
            <?= $this->Filesize->niceSize($file->size); ?>
          </td>
          <td>
            <button id="delete-file-<?= h($file['id']); ?>" type="button" class="btn btn-danger delete rounded-0" data-item="<?= h($file->id); ?>">
              <i class="fas fa-trash-alt"></i>
            </button>
            <?= $this->Html->link(
              $this->Html->tag('i','',['class' => 'fas fa-eye']),
              $file->path,
              ['class' => 'btn btn-primary rounded-0', 'target' => '_blank', 'escape' => false]
            ); ?>
          </td>
        </tr>
      <?php } ?>
    </tbody>
  </table>
</div>

<?php \Admiral\Admiral\Asset::scriptStart(); ?>
  <script>
    $(document).on('click',"[id^=delete-file]", function(){
      var fid = $(this).data("item");
      // Create new formdata and add the fileid to it
      var fd = new FormData();
      fd.append('fid', fid);

      // Send a delete request to the server
      $.ajax({
        url: '<?= $this->Url->build(['plugin'=>'Admiral/Admiral','controller'=>'Media','action'=>'delete']) ?>',
        type: 'POST',
        headers: {
          'X-CSRF-Token': <?= json_encode($this->request->getParam('_csrfToken')); ?>
        },
        data: fd,
        processData: false,
        contentType: false,
        success: function(resp) {
          switch(resp.status){
            case "success": {
              // Remove the item from the table
              $("#tr-" + fid).slideUp(300, function(){
                $(this).remove();
              });
              break;
            }
            case "failure": {
              // TODO
            }
          }
        }
      });
    });
  </script>
<?php \Admiral\Admiral\Asset::scriptEnd(); ?>