<div class="col-6">
  <?= $this->Form->create(); ?>
    <?php foreach($optionsGroups as $optionGroup){ ?>
      <h4><?= h($optionGroup['label']); ?></h4>
      <table class="table table-borderless">
        <?php foreach($optionGroup['options'][0] as $option){ ?>
          <?php
            $isValid = "";
            // Check whether this form field is valid or not
            if(!empty($validationErrors[$option['name']])){
              // Form field has an error
              $isValid = "is-invalid";
            }
          ?>
          <tr class="d-flex">
            <td class="col-4"><?= h($option['label']); ?></td>
            <td class="col-8">
              <?= $this->Form->control(
                $option['name'],
                [
                  "type"=>$option['type'],
                  "placeholder"=>$option['placeholder'],
                  'value'=>h($option['value']),
                  "label"=>false,
                  "class"=>"form-control rounded-0 " . $isValid
                ]
              ); ?>
            </td>
          </tr>
        <?php } ?>
      </table>
    <?php } ?>
    <?= $this->Form->button('Submit',["class"=>"btn btn-success rounded-0","value"=>__d('Admiral/Admiral','Submit')]); ?>
  <?= $this->Form->end(); ?>
</div>