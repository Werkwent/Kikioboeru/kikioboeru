<div class="row">
  <?php foreach($cells as $cell): ?>
    <div class="col-<?= $cell['columns'] ?? 3; ?>">
      <?= $this->Ui->card->start(); ?>
        <!-- Card header -->
        <?= $this->Ui->card->header($cell['title'], $cell['ns']); ?>

        <!-- Card body -->
        <?= $this->Ui->card->bodystart(); ?>
          <?= $this->cell($cell['ns'].'.'.$cell['cell']); ?>
        <?= $this->Ui->card->bodyend(); ?>
      <?= $this->Ui->card->end(); ?>
    </div>
  <?php endforeach; ?>
</div>