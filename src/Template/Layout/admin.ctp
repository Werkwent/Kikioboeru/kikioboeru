<!doctype html>
<html lang="en">
  <head>
    <!-- Page meta -->
    <?= $this->Html->charset(); ?>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>
      <?php if( !empty($title)) {echo $title . " - ";} ?> <?= h(\Cake\Core\Configure::read('Admiral.branding.name') ?? 'Admiral'); ?>
    </title>

    <!-- Stylesheets -->
    <?= $this->Html->css('Admiral/Admiral.vendor/bootstrap/bootstrap.css'); ?>
    <?= $this->Html->css('Admiral/Admiral.vendor/fontawesome/fontawesome.css'); ?>
    <?= $this->Html->css('Admiral/Admiral.styles.css'); ?>
    <?= $this->Html->css('Admiral/Admiral.vendor/toastr/toastr.css'); ?>
    <?= $this->Html->css('Admiral/Admiral.vendor/flag-icon/flag-icon.css'); ?>
  </head>
  <body class="bg-light">
    <!-- Top navbar -->
    <?= $this->element('Admiral/Admiral.Admin/Navs/top'); ?>

    <!-- Side nav -->
    <?= $this->element('Admiral/Admiral.Admin/Navs/side'); ?>

    <!-- Main Content -->
    <div class="d-flex">
      <div class="content p-4">
        <!-- Page title -->
        <h2 class="mb-4"><?= h($title) ?? '' ?></h2>

        <!-- Page content -->
        <?= $this->fetch('content'); ?>
      </div>
    </div>

    <!-- Scripts -->
    <?php foreach(\Admiral\Admiral\Asset::urls()['scripts'] as $script): ?>
      <?= $this->Html->script($script['url'], $script['options']); ?>
    <?php endforeach; ?>

    <?php foreach(\Admiral\Admiral\Asset::scripts() as $script): ?>
      <?= $script; ?>
    <?php endforeach; ?>
    <?= $this->Html->script('Admiral/Admiral.keepalive'); ?>
  </body>
</html>