<div class="row">
  <div class="col-md-3">
    <h4><?= __d('Admiral/Admiral','User\'s Details'); ?></h4>
    <?= $this->Flash->render('userDetails'); ?>
                
    <?= $this->Form->create($userDetailsForm); ?>
      <div class="form-group row">
        <div class="col-6">
          <?php
            $isValid = ""; # Clear our variable
            // Check whether this form field is valid or not
            if(!empty($validationErrors['firstname'])){
              // Form field has an error
              $isValid = "is-invalid";
            }
          ?>
          <?= $this->Form->control("firstname",["placeholder"=>__d('Admiral/Admiral',"User's First Name"),"label"=>false,"class"=>"form-control rounded-0 " . $isValid,"value" => $userData['users_detail']['firstname']]); ?>
          <?php 
            if(!empty($validationErrors['firstname'])){
              foreach($validationErrors['firstname'] as $error){ 
                ?>
                  <div class="invalid-feedback d-block"><?= $error; ?></div>
                <?php 
              } 
            }
          ?>
        </div>
        <div class="col-6">
          <?php
            $isValid = ""; # Clear our variable
            // Check whether this form field is valid or not
            if(!empty($validationErrors['lastname'])){
              // Form field has an error
              $isValid = "is-invalid";
            }
          ?>
          <?= $this->Form->control("lastname",["placeholder"=>__d('Admiral/Admiral',"User's Last Name"),"label"=>false,"class"=>"form-control rounded-0 " . $isValid,"value" => $userData['users_detail']['lastname']]); ?>
          <?php 
            if(!empty($validationErrors['lastname'])){
              foreach($validationErrors['lastname'] as $error){ 
                ?>
                  <div class="invalid-feedback d-block"><?= $error; ?></div>
                <?php 
              } 
            }
          ?>
        </div>
      </div>
      <div class="form-group row">
        <div class="col-12">
          <?= $this->Form->button('Submit',["class"=>"btn btn-success rounded-0","value"=>__d('Admiral/Admiral','Submit')]); ?>
          <?= $this->Form->button('Reset',["class"=>"btn btn-danger rounded-0","value"=>__d('Admiral/Admiral','Reset'),"type"=>"reset"]); ?>
        </div>
      </div>
      <?= $this->Form->hidden('action',["value" => "userDetails"]); ?>
    <?= $this->Form->end(); ?>
  </div>
</div>

<div class="row">
  <div class="col-12">
    <h4><?= __d('Admiral/Admiral','User\'s Role(s)'); ?></h4>
    <?= $this->Flash->render('userRoles'); ?>
    
    <table class="table table-striped">
      <thead>
        <tr class="d-flex">
          <th class="col">Role</th>
          <th class="col">Permissions</th>
          <th class="col-2">Actions</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach($roles as $role): ?>
          <tr class="d-flex">
            <td class="col"><?= h($role); ?></td>
            <td class="col">
              <?php foreach(\Admiral\Admiral\Permission::list($role) as $node): ?>
                <span class="badge badge-secondary rounded-0">
                  <?= h($node); ?>
                </span>
              <?php endforeach; ?>
            </td>
            <td class="col-2">
              <?php if($userData->hasRole($role)): ?>
                <button class="btn btn-danger rounded-0" data-action="remove-role" data-role="<?= $role; ?>">
                  <i class="fas fa-minus"></i>
                </button>
              <?php else: ?>
                <button class="btn btn-success rounded-0" data-action="add-role" data-role="<?= $role; ?>">
                  <i class="fas fa-plus"></i>
                </button>
              <?php endif; ?>
            </td>
          </tr>
        <?php endforeach; ?>
      </tbody>
      <thead>
        <tr class="d-flex">
          <th class="col">Role</th>
          <th class="col">Permissions</th>
          <th class="col-2">Actions</th>
        </tr>
      </thead>
    </table>

  <h4><?= __d('Admiral/Admiral','User\'s Permission(s)'); ?></h4>
  <div id="user-rights"></div>
  </div>
</div>

<script>
  $(function() {
    $(document).on('click',"[data-action=remove-role]", function(){
      // Save the button object
      var btn = this;

      // Create new formdata
      var fd = new FormData();
      fd.append('role', $(this).data("role"));
      fd.append('user', <?= h($userData->id); ?>);
      fd.append('action','removeRole')

      $.ajax({
        url: '<?= $this->Url->build(['plugin'=>'Admiral/Admiral','controller'=>'Users','action'=>'edit','id'=>$userData->id]) ?>',
        type: 'POST',
        headers: {
          'X-CSRF-Token': <?= json_encode($this->request->getParam('_csrfToken')); ?>
        },
        data: fd,
        processData: false,
        contentType: false,
        success: function(resp) {
          switch(resp.status) {
            case "failure":
              toastr.error(resp.message);
              break;
            case "success":
              toastr.success(resp.message);
              $(btn).removeClass('btn-danger');
              $(btn).addClass('btn-success');
              $($(btn).children()[0]).removeClass('fa-minus');
              $($(btn).children()[0]).addClass('fa-plus');

              $(btn).attr("data-action", "add-role");
              break;
          }

          getRights();
        }
      });
    });

    $(document).on('click',"[data-action=add-role]", function(){
      // Save the button object
      var btn = this;

      // Create new formdata
      var fd = new FormData();
      fd.append('role', $(this).data("role"));
      fd.append('user', <?= h($userData->id); ?>);
      fd.append('action','addRole');

      $.ajax({
        url: '<?= $this->Url->build(['plugin'=>'Admiral/Admiral','controller'=>'Users','action'=>'edit','id'=>$userData->id]) ?>',
        type: 'POST',
        headers: {
          'X-CSRF-Token': <?= json_encode($this->request->getParam('_csrfToken')); ?>
        },
        data: fd,
        processData: false,
        contentType: false,
        success: function(resp) {
          switch(resp.status) {
            case "failure":
              toastr.error(resp.message);
              break;
            case "success":
              toastr.success(resp.message);
              $(btn).removeClass('btn-success');
              $(btn).addClass('btn-danger');
              $($(btn).children()[0]).removeClass('fa-plus');
              $($(btn).children()[0]).addClass('fa-minus');
              $(btn).attr("data-action", "remove-role");
              break;
          }

          getRights();
        }
      });
    });
  });
</script>

<script>
  var rightsTemplate;

  function getRights(){
    $.ajax({
      url: '<?= $this->Url->build(['plugin'=>'Admiral/Admiral','controller'=>'Users','action'=>'getrights','id'=>$userData->id]) ?>',
      type: 'GET',
      headers: {
        'X-CSRF-Token': <?= json_encode($this->request->getParam('_csrfToken')); ?>
      }
    }).done(function(res) {
      console.log(res.result);
      $("#user-rights").empty();
      $.each(res.result, function(key, value) {
        var html = rightsTemplate({right: value});
        $("#user-rights").append(html);
      });
      //
    });
  }

  $(function(){
    // Compile the template for the rights
    rightsTemplate = Handlebars.compile($("#right-template").html());

    // Get all the rights
    getRights();
  });
</script>

<script id="right-template" type="text/x-handlebars-template">
  <span class="badge badge-secondary rounded-0">{{ right }}</span>
</script>