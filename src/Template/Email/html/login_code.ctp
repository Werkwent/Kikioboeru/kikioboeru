<p>
  Hello <?= h($username); ?>,
</p>
<p>
  Just a little moment ago, you have requested to login on <a href="<?= h(\Cake\Routing\Router::url('/', true)); ?>">GamingHQ</a>.<br />
  Here is the code you'll need to login.<br />
  Please don't give this code to anyone else.<br />
  <br />
  <?= h(strtoupper($code)); ?><br />
  <br />
  If it wasn't you whom had requested this code, it can be ignored.
</p>
<p>
  This email was send using Admiral's default template.<br />
  If you want to use your own template, please refer to the documentation.
</p>