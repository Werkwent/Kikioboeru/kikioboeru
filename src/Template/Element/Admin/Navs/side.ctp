<div class="d-flex sidebar bg-dark fixed-top flex-column">
  <ul class="list-unstyled">
    <?php foreach($menuSections as $section): ?>
      <!-- Section header -->
      <li class="sidebar-separator align-items-center h-100 pl-3">
        <small class="text-muted">
          <?= h(__d('Admiral/Admiral', $section['label'])); ?>
        </small>
      </li>

      <?php foreach($section['items'] as $item): ?>
        <?php if(empty($item['children'])): ?>
          <!-- Section item without children -->
          <li>
            <a href="<?= $this->Url->build($item['url']); ?>">
              <i class="<?= h($item['icon']); ?> fa-fw"></i> <?= h($item['label']); ?>
            </a>
          </li>
        <?php else: ?>
          <!-- Section item with children -->
          <li>
            <a href="#menu-dropdown-<?= h($item['label']); ?>" data-toggle="collapse">
              <i class="<?= h($item['icon']); ?> fa-fw"></i> <?= h($item['label']); ?>
            </a>
            <ul id="menu-dropdown-<?= h($item['label']); ?>" class="list-unstyled collapse">
              <?php foreach($item['children'] as $child): ?>
                <li>
                  <a href="<?= $this->Url->build($child['url']); ?>"><?= h($child['label']); ?></a>
                </li>
              <?php endforeach; ?>
            </ul>
          </li>
        <?php endif; ?>
      <?php endforeach; ?>
    <?php endforeach; ?>
  </ul>
  <div class="mt-auto sidebar-copyright align-items-center text-white font-weight-bold text-center text-justify px-2 pt-2">
    <ul class="px-0 py-0">
      <li>Admiral</li>
      <li class="font-weight-light text-muted">"Maiden Voyage"</li>
      <li class="font-weight-light text-muted">
        <?= h(\Admiral\Admiral\PackageInfo::selectPlugin('admiral/admiral')['version']); ?>
      </li>
    </ul>
  </div>
</div>