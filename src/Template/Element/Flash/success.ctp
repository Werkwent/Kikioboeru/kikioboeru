<?php
if (!isset($params['escape']) || $params['escape'] !== false) {
    $message = h($message);
}
?>

<div class="alert alert-success rounded-0" role="alert">
  <?= $message ?>
</div>