<?php
namespace Admiral\Admiral\View\Helper;

use Cake\View\Helper;
use Cake\View\View;
use Cake\View\Helper\HtmlHelper;

use Admiral\Admiral\View\Helper\Ui\{
  Card,
  TextEditor,
  Table,
  Checkbox
};

class UiHelper extends HtmlHelper {
  public function __construct() {
    $this->card = new Card();
    $this->texteditor = new TextEditor();
    $this->table = new Table();
    $this->checkbox = new Checkbox();
  }  
}
