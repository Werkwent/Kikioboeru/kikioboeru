<?php
namespace Admiral\Admiral\View\Helper;

use Cake\View\Helper;
use Cake\View\View;

class FilesizeHelper extends Helper {
  public function niceSize($bytes) {
    $units = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
    $bytes = max($bytes, 0);
    $pow = floor(($bytes ? log($bytes) : 0) / log(1000));
    $pow = min($pow, count($units) - 1);
    $bytes /= pow(1000, $pow);
    return round($bytes, 2) . ' ' . $units[$pow];
  }
}
