<?php 
namespace Admiral\Admiral\View\Cell;

use Cake\View\Cell;
use Cake\I18n\Time;
use Cake\Database\Expression\QueryExpression;
use Cake\ORM\Query;

class UsersCountCell extends Cell {
  public $title;

  public function initialize() {
    parent::initialize();

    $this->title = __('Registered Users');
  }

  public function display(){
    $this->loadModel('Admiral/Admiral.Users');

    $data = [];

    $data['total'] = $this->Users->find()->count();
    $data['new'] =  $this->Users->find()
      ->where(function(QueryExpression $exp, Query $q) {
        $now = Time::now();
        return $exp->between('Users.created', $now->subMonth(1), $now);
      })
      ->count();

    $this->set('data', $data);
  }
}