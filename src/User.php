<?php
  namespace Admiral\Admiral;

  use Admiral\Admiral\Session;

  class User {
    public static function get() {
      return Session::get()->read('Auth.User');
    }

    public static function set($value) {
      return Session::get()->write('Auth.User', $value);
    }
  }