<?php
namespace Admiral\Admiral\Controller;

use Cake\Routing\Router;
use Cake\Event\Event;
use Cake\Filesystem\{
  Folder,
  File
};
use Cake\I18n\Time;
use Admiral\Admiral\Controller\AppController;
use Admiral\Admiral\Permission;
use Admiral\Admiral\User;
use FinlayDaG33k\FileUpload\{
  FileUpload,
  Network\SimpleRequest
};

class MediaController extends AppController {
  public function initialize(): void {
    parent::initialize();

    // Load required models
    $this->loadModel('Admiral/Admiral.Users');
    $this->loadModel('Admiral/Admiral.UsersDetails');
    $this->loadModel('Admiral/Admiral.Options');
    $this->loadModel('Admiral/Admiral.Media');

    // Check whether the user is logged in or not
    if(!User::get()) {
      // User is not logged in
      // Check whether we are on the login page
      if($this->request->getParam('action') != 'login') {
        // We are not on the login page
        // Redirect the user to the login page
        $this->redirect([
          'controller' => 'Users',
          'action' => 'login',
          'redir' => Router::url([
            'controller' => $this->request->params['controller'], 
            'action' => $this->request->params['action']
          ])
        ]);
      }
    }

    // Check if the user has the right permission
    if(!Permission::check('admiral.admiral.cms.access', 1)){
      // User does not have the right permission
      $this->redirect([
        'controller' => 'Users',
        'action' => 'my_account',
        'my-account'
      ]);
    }
    
    $this->viewBuilder()->setLayout('admin');
  }

  public function index(){
    // Get all the media
    $media = $this->Media->find()->all();

    $this->set('title', __d('Admiral/Admiral','Media Library'));
    $this->set('media', $media);
  }

  public function add(){
    // Get the current time
    $time = Time::now();

    // Get the paths to the upload dirs and create parents as needed
    $uploads = new Folder(WWW_ROOT . "uploads" . DS . $time->i18nFormat('yyyy') . DS . $time->i18nFormat('LLL'), true);
    $temp = new Folder(TMP . DS . 'uploads', true);

    if($this->request->is('post')) {
      // Check whether the user is allowed to upload media
      if(!Permission::check('admiral.admiral.media.add', 1)) {
        $this->response->statusCode(403);
        return $this->response
          ->withType('application/json')
          ->withStringBody(json_encode([
            'status' => 'failure',
            'message' => 'You are not allowed to access this method'
          ]));
      }

      // Handle the request
      $request = new SimpleRequest();
      $fu = new FileUpload($request);
      $fu->tempFolder = $temp->path;
      $fu->uploadFolder = $uploads->path;
      $res = $fu->process();

      // "wait" for the upload to be complete
      if($fu->isUploadComplete()) {
        // Open the file to get some info from it
        $file = new File($fu->getFilepath());

        $media = $this->Media->newEntity();
        $media->filename = $file->name;
        $media->path = "/uploads/" . $time->i18nFormat('yyyy') . '/' . $time->i18nFormat('LLL').'/'. $file->name;
        $media->created = $time;
        $media->modified = $time;
        $media->size = $file->size();
        $media->type = explode("/",$file->mime())[0];;

        if(!$this->Media->save($media)) {
          // Something went wrong

          //TODO: delete the file

          $this->response->statusCode(500);
          return $this->response
            ->withType('application/json')
            ->withStringBody(json_encode([
              'status' => 'failure',
              'message' => 'Something went wrong while trying to upload the file'
            ]));
        }

        return $this->response
          ->withType('application/json')
          ->withStringBody(json_encode([
            'status' => 'success',
            'message' => 'File has been uploaded!'
          ]));
      }

      $this->response->statusCode($res);
    }

    if($this->request->getQuery('resumableIdentifier')) {
      // Handle the request
      $request = new SimpleRequest();
      $fu = new FileUpload($request);
      $fu->tempFolder = $temp->path;
      $fu->uploadFolder = $uploads->path;
      $res = $fu->process();

      $this->response->statusCode($res);
      return;
    }

    $this->set('title', __d('Admiral/Admiral','Add Media'));
  }

  public function delete(){
    // Check if the user is logged in
    if(!User::get()) {
      // User is not logged in
      return $this->response
        ->withType('application/json')
        ->withStringBody(json_encode([
          'status' => 'failure',
          'message' => 'You are not allowed to access this method'
        ]));
    }

    // Check if the user has the permission to delete media
    if(!Permission::check('admiral.admiral.media.delete',1)) {
      // User does not have the permission to delete media
      return $this->response
        ->withType('application/json')
        ->withStringBody(json_encode([
          'status' => 'failure',
          'message' => 'You are not allowed to access this method'
        ]));
    }

    // Get the file entity and store it's path for later
    $file = $this->Media
      ->findById($this->request->getData('fid'))
      ->first();
    $path = $file->path;
    
    // Delete the file entity from the database and check if deletion was successful
    if(!$this->Media->delete($file)) {
      // Deletion went wrong
      return $this->response
        ->withType('application/json')
        ->withStringBody(json_encode([
          'status' => 'failure',
          'message' => 'File could not be deleted!'
        ]));
    }

    // Create the file object
    $file = new File(WWW_ROOT . $path);
    
    // Delete the file
    if(!$file->delete()) {
      return $this->response
        ->withType('application/json')
        ->withStringBody(json_encode([
          'status' => 'failure',
          'message' => 'File could not be deleted!'
        ]));
    }

    // Everything went right
    return $this->response
      ->withType('application/json')
      ->withStringBody(json_encode([
        'status' => 'success',
        'message' => 'File has been deleted!'
      ]));
  }
}