<?php
  namespace Admiral\Admiral\Controller\Component;

  use Cake\Controller\Component;
  use Cake\Core\Configure;
  use Cake\ORM\TableRegistry;

  class DashboardComponent extends Component {
    public function registerCell(array $cell){
      // Read the dashboard cells from the Configure
      $cells = Configure::read('Admiral\Admiral.dashboardCells');
      
      // Add our cell
      $cells[] = $cell;

      // Save the dashboard cells
      Configure::write('Admiral\Admiral.dashboardCells',$cells);

      return $this;
    }
  }