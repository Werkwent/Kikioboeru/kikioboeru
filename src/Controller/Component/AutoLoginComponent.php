<?php
  namespace Admiral\Admiral\Controller\Component;

  use Cake\Controller\Component;
  use Cake\ORM\TableRegistry;
  use Cake\Utility\Security;
  use Cake\I18n\Time;

  class AutoLoginComponent extends Component {
    private $AuthTokens;
    private $Users;

    public function __construct() {
      $this->AuthTokens = TableRegistry::get('Admiral/Admiral.AuthTokens');
      $this->Users = TableRegistry::get('Admiral/Admiral.Users');
    }

    public function login($token){
      // Check if the cookie is set
      if($token){
        // cookie is present
        // check in database if token is found
        $query = $this->AuthTokens->findByToken($token)->first();
        if($query){
          // A valid token has been found
          // Login the user
          $user_id = $query->toArray()['user_id'];
          $user = $this->Users->findById($user_id)->contain('Roles')->first();

          return $user;
        }
        return false;
      }
      return false;
    }

    public function addToken($user_id) {
      // generate a token string
      $token = Security::randomString(64);

      // Get the timestamps
      $now = Time::now();
      $expires = $now->modify('+10 days');

      // Create the token entity
      $entity = $this->AuthTokens->newEntity();
      $entity->token = $token;
      $entity->created = $now;
      $entity->modified = $now;
      $entity->expires = $expires;
      $entity->user_id = $user_id;

      // Save the token
      return [
        'success' => $this->AuthTokens->save($entity),
        'token' => $token,
        'expires' => $expires
      ];
    }
  }