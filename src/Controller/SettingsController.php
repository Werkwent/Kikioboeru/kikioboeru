<?php
  namespace Admiral\Admiral\Controller;

  use Admiral\Admiral\Controller\AppController;
  Use Admiral\Admiral\Permission;
  use Cake\Event\Event;
  use Cake\Core\{
    Plugin,
    Configure
  };
  use Cake\Routing\Router;
  use Admiral\Admiral\Settings;
  
  class SettingsController extends AppController {
    public function beforeFilter(Event $event) {
      $this->Auth->allow(['index']);
      $this->Auth->autoRedirect = false;
    }

    public function initialize() {
      parent::initialize();

      // Load required models
      $this->loadModel('Users');
      $this->loadModel('Options');

      if(!$this->Auth->user()) {
        // User is not logged in
        // Redirect the user to the login page
        return $this->redirect([
          'controller' => 'Users',
          'action' => 'login',
          'redir' => Router::url([
            'controller' => $this->request->params['controller'], 
            'action' => $this->request->params['action']
          ])
        ]);
      }

      // Check whether the user is allowed to access the CMS
      if(!Permission::check('admiral.admiral.cms.access',1)) {
        // User does not have the right permission
        // redirect to the general my-account page
        return $this->redirect(['controller' => 'Users', 'action' => 'my_account', 'my-account']);
      }

      // Check if the suer is allow to edit settings
      if(!Permission::check('admiral.admiral.settings.edit', 1)) {
        // User does not have the right permission
        // Redirect to the main dashboard
        return $this->redirect(['controller' => 'Admin', 'action' => 'index']);
      }
    
      $this->viewBuilder()->setLayout('admin');
    }

    public function index() {
      $this->set('title', __d('Admiral/Admiral','Settings'));

      // Get all the options from the database
      $options = $this->Options->find()->all();

      // Check if we're dealing with a post request
      if($this->request->is('post')) {
        foreach($this->request->getData() as $option => $value) {
          Settings::saveOption($option,$value);
        }
      }

      // Pass all options to the view
      $this->set('optionsGroups', Settings::getOptions());
    }
  }