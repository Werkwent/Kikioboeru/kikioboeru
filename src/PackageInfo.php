<?php
  namespace Admiral\Admiral;

  use Cake\Filesystem\File;
  use Cake\Collection\Collection;

  class PackageInfo {
    private static $installed = [];

    public static function load() {
      // Make sure we didn't already load our data
      if(self::$installed) return;

      // Get the file in which we should get our data
      $file = new File(ROOT . DS . 'vendor' . DS . 'composer' . DS . 'installed.json');

      // Make sure the file exists
      if(!$file->exists()) throw new \Exception('Could not find "installed.json", please run "composer install" first');

      // Read the file and parse it
      $content = $file->read();
      $parsed = \json_decode($content, true);
      self::$installed = new Collection($parsed);
    }

    public static function selectPlugin(string $name) {
      $filtered = self::$installed->filter(function($package) use ($name) {
        return $package['name'] == $name;
      });
      return $filtered->first();
    }
  }