<?php


  namespace Admiral\Admiral\GraphQL\Datasource;

  use Admiral\Admiral\User;
  use Cake\Core\Configure;
  use Cake\Filesystem\File;
  use \Imagick;

  class UserDatasource {
    public function updateAvatar(array $args = []) {
      // Make sure we're logged in
      $user = User::get();
      if(!$user) return [
        'success' => false,
        'message' => 'You need to be logged in to change your avatar!',
      ];

      // Get the path prefix
      $prefix = (!empty(Configure::read('App.imageBaseUrl'))) ? Configure::read('App.imageBaseUrl') . DS : '';

      // If there is no data, remove the avatar for the user
      if(empty($args['data'])) {
        $file = new File(WWW_ROOT . DS . $prefix . 'user-avatars' . DS . $user->username . '.webp', false);
        $file->delete();
        return [
          'success' => true,
          'message' => 'Avatar has been removed!',
        ];
      }

      // Decode the image
      $decoded = base64_decode(str_replace(' ','+',explode(',', $args['data'])[1]));

      // Check if the image is below 5MB
      // If so, turn it into a WebP
      // Else, throw an exception
      try {
        $imagick = new Imagick();
        $imagick->readImageBlob($decoded);
        if($imagick->getImageLength() > 5 * 1024 * 1024) throw new \Exception('It appears your new avatar is too big...<br />Please stick to <b>5MB</b> or smaller!');
        $imagick->setImageFormat('webp');
        $imagick->setImageCompressionQuality(60);
      } catch (\Exception $e) {
        return [
          'success' => false,
          'message' => $e->getMessage(),
        ];
      }


      // Open a Filehandle
      $file = new File(WWW_ROOT . DS . $prefix . 'user-avatars' . DS . $user->username . '.webp', true);
      if(!$file->write($imagick)) return [
        'success' => false,
        'message' => 'Avatar could not be updated!',
      ];

      return [
        'success' => true,
        'message' => 'Avatar has been updated!',
      ];
    }
  }
