<?php
  namespace Admiral\Admiral\GraphQL\Datasource;

  use Admiral\Admiral\User;
  use Admiral\Admiral\Session;
  use Admiral\Admiral\Webauthn\Repository\DatabaseRepository;
  use Admiral\Admiral\Webauthn\AlgorithmFactory;
  use Admiral\Admiral\Webauthn\Psr17Creator;
  use Admiral\Admiral\Webauthn\Challenge;
  use Admiral\Admiral\Webauthn\RelyingParty;
  use Admiral\Admiral\Webauthn\UserEntity;
  use Admiral\Admiral\Webauthn\PublicKey;

  use Cake\ORM\TableRegistry;
  use Webauthn\AuthenticatorSelectionCriteria;
  use Webauthn\PublicKeyCredentialDescriptor;
  use Webauthn\PublicKeyCredentialCreationOptions;
  use Webauthn\PublicKeyCredentialRequestOptions;
  use Webauthn\PublicKeyCredentialSource;
  use Webauthn\AuthenticatorAssertionResponse;
  use Webauthn\AttestationStatement\AttestationStatementSupportManager;
  use Webauthn\AttestationStatement\NoneAttestationStatementSupport;
  use Webauthn\AttestationStatement\AttestationObjectLoader;
  use Webauthn\AttestationStatement\PackedAttestationStatementSupport;
  use Webauthn\PublicKeyCredentialLoader;
  use Webauthn\AuthenticatorAttestationResponseValidator;
  use Webauthn\TokenBinding\IgnoreTokenBindingHandler;
  use Webauthn\AuthenticationExtensions\ExtensionOutputCheckerHandler;
  use Webauthn\AuthenticatorAttestationResponse;
  use Webauthn\AuthenticatorAssertionResponseValidator;

  class WebauthnDatasource {
    private $timeout = 60_000;

    public function registrationChallenge($args) {
      // Check if the user if logged in
      // Throw an exception if not
      if(!$user = User::get()) throw new \Exception('User is not authenticated!');

      // Get our Relying Party
      $relyingParty = (new RelyingParty)->getParty();

      // Get our User Entity
      $userEntity = (new UserEntity($user))->getUser();

      // Generate a new challenge
      $challenge = new Challenge(16);

      // Get our public key algorithms
      $publicKeyCredentialParametersList = (new PublicKey)->getPublicKeyAlgorithms();

      // Exclude already registered public keys
      $excludedPublicKeyDescriptors = [];

      // Build our options
      $challengeData = new PublicKeyCredentialCreationOptions(
        $relyingParty,
        $userEntity,
        $challenge->getChallengeHex(),
        $publicKeyCredentialParametersList,
        $this->timeout,
        $excludedPublicKeyDescriptors,
        new AuthenticatorSelectionCriteria(),
        PublicKeyCredentialCreationOptions::ATTESTATION_CONVEYANCE_PREFERENCE_NONE,
        null // Extensions
      );

      // Store our data for verification
      $session = Session::get();
      $session->write('Webauthn data', [
        'user' => $userEntity,
        'challenge' => $challengeData,
        'tokenName' => $args['tokenName'] ?? '',
      ]);

      // Return our challenge data
      return $challengeData;
    }

    public function registerToken($args = []) {
      // Check if the user if logged in
      // Throw an exception if not
      if(!$user = User::get()) return [
        'success' => false,
        'message' => 'You must be logged in to register your security key',
      ];

      // Create our Algorithm Factory and Manager
      $alforithmFactory = new AlgorithmFactory();
      $algorithmManager = $alforithmFactory->createManager(['RS256', 'RS512', 'PS256', 'PS512', 'ES256', 'ES512', 'Ed25519']);

      // Create most of the needed stuff
      // TODO: Clean this up
      $attestationStatementSupportManager = new AttestationStatementSupportManager();
      $attestationStatementSupportManager->add(new NoneAttestationStatementSupport());
      $attestationStatementSupportManager->add(new PackedAttestationStatementSupport($algorithmManager));
      $attestationObjectLoader = new AttestationObjectLoader($attestationStatementSupportManager);
      $publicKeyCredentialLoader = new PublicKeyCredentialLoader($attestationObjectLoader);
      $publicKeyCredentialSourceRepository = new DatabaseRepository();
      $tokenBindingHandler = new IgnoreTokenBindingHandler();
      $extensionOutputCheckerHandler = new ExtensionOutputCheckerHandler();

      // Load our public key credential
      try {
        $publicKeyCredential = $publicKeyCredentialLoader->loadArray($args['publicKeyCredential']);
      } catch(\InvalidArgumentException $e) {
        return [
          'success' => false,
          'message' => $e->getMessage(),
        ];
      }

      // Get the challenge data
      $session = Session::get();
      $publicKeyCredentialCreationOptions = $session->read('Webauthn data')['challenge'];
      
      // Get our response
      // Make sure it's an instance of \Webauthn\AuthenticatorAttestationResponse
      $authenticatorAttestationResponse = $publicKeyCredential->getResponse();
      if (!$authenticatorAttestationResponse instanceof AuthenticatorAttestationResponse) {
        throw new \Exception('"$authenticatorAttestationResponse" must be an instance of "Webauthn\\AuthenticatorAttestationResponse"');
      }

      // Try to validate the response using PSR-17
      try {
        // Create a new PSR17 server request
        $creator = (new Psr17Creator())->createServerRequest();

        // Create our response validator
        $authenticatorAttestationResponseValidator = new AuthenticatorAttestationResponseValidator(
          $attestationStatementSupportManager,
          $publicKeyCredentialSourceRepository,
          $tokenBindingHandler,
          $extensionOutputCheckerHandler
        );

        // Validate the key
        $publicKeyCredentialSource = $authenticatorAttestationResponseValidator->check(
          $authenticatorAttestationResponse,
          $publicKeyCredentialCreationOptions,
          $creator->fromGlobals(),
          ['localhost']
        );
      } catch(\InvalidArgumentException $e) {
        return [
          'success' => false,
          'message' => $e->getMessage(),
        ];
      }

      // Save our credential to the store
      $publicKeyCredentialSourceRepository->saveCredentialSource($publicKeyCredentialSource, $session->read('Webauthn data')['tokenName']);

      // Return our success
      return [
        'success' => true,
        'message' => 'Your security key has been registered and can now be used to login!',
      ];
    }

    public function loginChallenge($args) {
      // Get the UsersTable
      $usersTable = TableRegistry::getTableLocator()->get('Users');

      // Find the user in our database
      // Then check if it exists
      $user = $usersTable->findByUsername($args['username'])->first();
      if(!$user) return ['success' => false, 'message' => 'User could not be found'];

      // Get our user entity
      $userEntity = (new UserEntity($user))->getUser();

      // Create a new DatabaseRepository instance
      $publicKeyCredentialSourceRepository = new DatabaseRepository();
    
      // Get our authenticators
      $registeredAuthenticators = $publicKeyCredentialSourceRepository->findAllForUserEntity($userEntity);

      // Get the allowed credentials
      $allowedCredentials = array_map(
        static function (PublicKeyCredentialSource $credential): PublicKeyCredentialDescriptor {
            return $credential->getPublicKeyCredentialDescriptor();
        },
        $registeredAuthenticators
      );

      // Generate a challenge
      $challenge = new Challenge(16);

      // Create our challenge data
      $publicKeyCredentialRequestOptions = new PublicKeyCredentialRequestOptions(
        $challenge->getChallengeHex(),
        $this->timeout,
        $_SERVER['HTTP_HOST'],
        $allowedCredentials,
        PublicKeyCredentialRequestOptions::USER_VERIFICATION_REQUIREMENT_DISCOURAGED
      );

      // Store our data for verification
      $session = Session::get();
      $session->write('Webauthn data', [
        'user' => $userEntity,
        'challenge' => $publicKeyCredentialRequestOptions
      ]);

      // Return our challenge
      return $publicKeyCredentialRequestOptions;
    }

    public function loginToken($args = []) {
      // Get our session
      $session = Session::get();

      // Get our user
      $user = $session->read('Webauthn data')['user'];
      if(!$user) return ['success' => false, 'message' => 'Could not find user in session'];

      // Get our challenge
      $publicKeyCredentialRequestOptions = $session->read('Webauthn data')['challenge'];

      // Create our Algorithm Factory and Manager
      $alforithmFactory = new AlgorithmFactory();
      $algorithmManager = $alforithmFactory->createManager(['RS256', 'RS512', 'PS256', 'PS512', 'ES256', 'ES512', 'Ed25519']);

      // Create most of the needed stuff
      // TODO: Clean this up
      $attestationStatementSupportManager = new AttestationStatementSupportManager();
      $attestationStatementSupportManager->add(new NoneAttestationStatementSupport());
      $attestationStatementSupportManager->add(new PackedAttestationStatementSupport($algorithmManager));
      $attestationObjectLoader = new AttestationObjectLoader($attestationStatementSupportManager);
      $publicKeyCredentialLoader = new PublicKeyCredentialLoader($attestationObjectLoader);
      $publicKeyCredentialSourceRepository = new DatabaseRepository();
      $tokenBindingHandler = new IgnoreTokenBindingHandler();
      $extensionOutputCheckerHandler = new ExtensionOutputCheckerHandler();


      // Load our public key credential
      try {
        $publicKeyCredential = $publicKeyCredentialLoader->loadArray($args['publicKeyCredential']);
      } catch(\InvalidArgumentException $e) {
        return [
          'success' => false,
          'message' => $e->getMessage(),
          'stacktrace' => $e->getTrace(),
        ];
      }

      $authenticatorAssertionResponse = $publicKeyCredential->getResponse();
      if (!$authenticatorAssertionResponse instanceof AuthenticatorAssertionResponse) {
        throw new \Exception('"$authenticatorAttestationResponse" must be an instance of "Webauthn\\AuthenticatorAttestationResponse"');
      }

      // Try to validate the response
      try {
        // Create a new PSR17 server request
        $creator = (new Psr17Creator())->createServerRequest();

        // Create our response validator
        $authenticatorAssertionResponseValidator = new AuthenticatorAssertionResponseValidator(
          $publicKeyCredentialSourceRepository,
          $tokenBindingHandler,
          $extensionOutputCheckerHandler,
          $algorithmManager
        );

        // Validate the key
        $publicKeyCredentialSource = $authenticatorAssertionResponseValidator->check(
          $publicKeyCredential->getRawId(),
          $authenticatorAssertionResponse,
          $publicKeyCredentialRequestOptions,
          $creator->fromGlobals(),
          $user->getId()
        );
      } catch(\InvalidArgumentException $e) {
        return [
          'success' => false,
          'message' => $e->getMessage(),
        ];
      } catch(\Throwable $throwable) {
        return [
          'success' => false,
          'message' => $throwable->getMessage(),
        ];
      }

      // No exceptions along the way
      // Get our user entity from the table
      $usersTable = TableRegistry::getTableLocator()->get('Admiral/Admiral.Users');
      $user = $usersTable->findByUsername($user->getDisplayName())->contain('Roles')->first();
      if(!$user) return ['success' => false, 'message' => 'User could not be found'];

      // Set our user
      $session->write('Auth.User', $user);

      // Delete our authentication data
      $session->delete('Webauthn data');

      // Return our status
      return [
        'success' => true,
        'message' => 'Authentication succeeded! You\'ll be redirected any second now...',
      ];
    }

    public function getAllForUser($args = []) {
       // Check if the user if logged in
      // Throw an exception if not
      if(!$user = User::get()) throw new \Exception('User is not authenticated!');

      // Get our User Entity
      $userEntity = (new UserEntity($user))->getUser();

      // Initialize our DatabaseRepository
      $publicKeyCredentialSourceRepository = new DatabaseRepository($userEntity);

      // Find all keys
      $keys = $publicKeyCredentialSourceRepository->findAllForUserEntity($userEntity);

      // Return all keys
      return $keys;
    }
  }