<?php
  namespace Admiral\Admiral\GraphQL\Datasource;

  use Admiral\Admiral\Email;
  use Admiral\Admiral\Session;
  use Admiral\Admiral\Webauthn\Challenge;


  use Cake\ORM\TableRegistry;
  use Cake\I18n\Time;
  use Cake\Core\Configure;

  class EmailLoginDatasource {
    private $usersTable;
    private $codesTable;

    public function __construct() {
      $this->usersTable = TableRegistry::getTableLocator()->get('Admiral/Admiral.Users');
      $this->codesTable = TableRegistry::getTableLocator()->get('LoginOtp');
    }

    public function createCode(string $username) {
      // Make sure this user exists either by username or email
      // Don't tell the front-end whether it exists or not
      $user = $this->usersTable->find()->where(['OR' => ['username' => $username, 'email' => $username]])->first();
      if(!$user) {
        return [
          'success' => true,
          'message' => 'If a user was found, you will receive your code in your email shortly!',
        ];
      }

      // Generate a code
      $code = (new Challenge(3))->getChallengeHex();

      // Put the code in the database
      $entity = $this->codesTable->newEntity([
        'user_id' => $user->id,
        'code' => $code,
        'created' => new Time(),
      ]);
      $status = $this->codesTable->save($entity);

      // Check the status
      // Return an error if need be
      if(!$status) {
        return [
          'success' => false,
          'message' => 'Something went wrong while sending your token...<br />Please try again later!',
          'error' => 'Could not insert code into database',
        ];
      }

      // Build our email
      $email = new Email();
      $email->set('to', $user->email);
      if($subject = Configure::read('Admiral.email.loginSubject')) {
        $email->set('subject', $subject);
      } else {
        $email->set('subject', 'Your login code for ' . env('APP_NAME'));
      }

      $email->set('template', 'Admiral/Admiral.login-code');
      $email->set('format', 'html');
      $email->set('viewVars', [
        'code' => $code,
        'username' => $user->username,
      ]);

      // Send the email
      // Return an error if need be
      try {
        $email->send();
      } catch(\Exception $e) {
        $resp = [
          'success' => false,
          'message' => 'Something went wrong while sending your token...\r\nPlease try again later!',
          'error' => $e->getMessage(),
        ];

        return $resp;
      }

      // Everything OK
      return [
        'success' => true,
        'message' => 'If a user was found, you will receive your code in your email shortly!',
      ];
    }

    public function login(string $username, string $code) {
      // Check if the code exists
      $entry = $this->codesTable->find()
        ->where(['code' => strtolower($code)])
        ->first();
      if(!$entry) {
        return [
          'success' => false,
          'message' => 'The code provided was invalid',
        ];
      }

      // Check if the user associated exists
      $user = $this->usersTable->find()
        ->where(['id' => $entry->user_id])
        ->contain('Roles')
        ->first();
      if(!$user) {
        return [
          'success' => false,
          'message' => 'The code provided was invalid',
          ];
      }

      // Delete the token
      $this->codesTable->delete($entry);

      // Login the user
      (Session::get())->write('Auth.User', $user);

      return [
        'success' => true,
        'message' => 'Authentication succeeded! You\'ll be redirected any second now...',
      ];
    }
  }
