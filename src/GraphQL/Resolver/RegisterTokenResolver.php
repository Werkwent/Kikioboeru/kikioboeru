<?php
  namespace Admiral\Admiral\GraphQL\Resolver;

  use Admiral\Admiral\GraphQL\Datasource\WebauthnDatasource;

  use GraphQL\Type\Definition\ResolveInfo;

  class RegisterTokenResolver {
    protected $webauthnDatasource;

    public function resolver($rootValue, $args, $context, ResolveInfo $info) {
      // Build our method name
      $method = 'resolve' . ucfirst($info->fieldName);

      // Check if our method exists
      // If so, resolve using the method
      // Else, resolve using the field itself
      if (method_exists($this, $method)) {
        // Initialize the Datasource if not done yet
        if(!$this->webauthnDatasource) $this->webauthnDatasource = new WebauthnDatasource();
        
        // Resolve the field and return the info
        return $this->{$method}($rootValue, $args, $context, $info);
      } else {
        return $rootValue->{$info->fieldName};
      }
    }

    public function resolveRegisterToken($rootValue, $args, $context, $info) {
      return $this->webauthnDatasource->registerToken($args);
    }
  }