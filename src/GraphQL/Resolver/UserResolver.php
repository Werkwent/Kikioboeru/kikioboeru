<?php


  namespace Admiral\Admiral\GraphQL\Resolver;

  use Admiral\Admiral\GraphQL\Datasource\UserDatasource;
  use GraphQL\Type\Definition\ResolveInfo;

  class UserResolver {
    protected $datasource;

    public function __construct() {
      $this->datasource = new UserDatasource();
    }

    public function updateAvatar($rootValue, $args, $context, $info) {
      return $this->datasource->updateAvatar($args);
    }
  }
