<?php
  namespace Admiral\Admiral\GraphQL\Resolver;

  use Admiral\Admiral\GraphQL\Datasource\EmailLoginDatasource;

  class EmailLoginResolver {
    private $datasource;

    public function __construct() {
      $this->datasource = new EmailLoginDatasource();
    }

    public function getToken($rootValue, $args, $context, $info) {
      return $this->datasource->createCode($args['username']);
    }

    public function login($rootValue, $args, $context, $info) {
      return $this->datasource->login($args['username'], $args['code']);
    }
  }