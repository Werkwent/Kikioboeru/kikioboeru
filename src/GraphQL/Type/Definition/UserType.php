<?php
  namespace Admiral\Admiral\GraphQL\Type\Definition;

  use Admiral\GraphQL\Types;

  class UserType {
    public function config() {
      return [
        'name' => 'User',
        'fields' => function() {
          return [
            'id' => [
              'type' => Types::get('int'),
              'description' => 'User id',
            ],
            'username' => [
              'type' => Types::get('string'),
              'description' => 'The username of the user'
            ],
            'firstname' => [
              'type' => Types::get('string'),
              'description' => 'The firstname of the user'
            ],
            'lastname' => [
              'type' => Types::get('string'),
              'description' => 'The lastname of the user'
            ],
            'email' => [
              'type' => Types::get('string'),
              'description' => 'The email of the user'
            ],
            'created' => [
              'type' => Types::get('string'),
              'description' => 'The datetime this user was created'
            ],
          ];
        }
      ];
    }
  }