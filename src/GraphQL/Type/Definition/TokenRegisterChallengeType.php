<?php
  namespace Admiral\Admiral\GraphQL\Type\Definition;

  use Admiral\Admiral\GraphQL\Datasource\WebauthnDatasource;

  use Admiral\GraphQL\Types;
  use GraphQL\Type\Definition\{
    ObjectType,
    ResolveInfo
  };

  class TokenRegisterChallengeType {
    public function config() {
      return [
        'name' => "TokenRegisterChallenge",
        'fields' => [
          'rp' => new ObjectType([
            'name' => 'RP',
            'description' => 'Data about the Relying party',
            'fields' => [
              'id' => [
                'type' => Types::get('string'),
                'description' => 'ID of the relying party (most commonly the domain)'
              ],
              'name' => [
                'type' => Types::get('string'),
                'description' => 'Name of the relying party'
              ],
              'icon' => [
                'type' => Types::get('string'),
                'description' => '??? (will always return null)'
              ]
            ]
          ]),
          'user' => new ObjectType([
            'name' => 'WebauthnUser',
            'description' => 'Data about the user we try to register',
            'fields' => [
              'name' => [
                'type' => Types::get('string'),
                'description' => 'The email of the user',
              ],
              'id' => [
                'type' => Types::get('string'),
                'description' => 'Base64 encoded user id',
              ],
              'displayName' => [
                'type' => Types::get('string'),
                'description' => 'The username of the user',
              ],
              'avatar' => [
                'type' => Types::get('string'),
                'description' => 'The url of the user\'s avatar',
              ],
            ]
          ]),
          'pubKeyCredParams' => [
            'type' => Types::listOf(new ObjectType([
              'name' => 'PubKeyCredParam',
              'fields' => [
                'type' => [
                  'type' => Types::get('string'),
                  'description' => '',
                ],
                'alg' => [
                  'type' => Types::get('int'),
                  'description' => '',
                ]
              ]
            ])),
            'description' => '',
          ],
          'authenticatorSelection' => new ObjectType([
            'name' => 'AuthenticatorSelection',
            'fields' => [
              'requireResidentKey' => [
                'type' => Types::get('boolean'),
                'description' => '',
              ],
              'userVerification' => [
                'type' => Types::get('string'),
                'description' => '',
              ],
            ],
          ]),
          'challenge' => [
            'type' => Types::get('string'),
            'description' => 'Challenge bytes encoded in hexadecimal'
          ],
          'attestation' => [
            'type' => Types::get('string'),
            'description' => '',
          ],
          'timeout' => [
            'type' => Types::get('int'),
            'description' => 'The time the client has to respond',
          ],
        ],
      ];
    }
  }