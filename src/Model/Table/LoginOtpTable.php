<?php
namespace Admiral\Admiral\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * LoginOtp Model
 *
 * @property \Admiral\Admiral\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 *
 * @method \Admiral\Admiral\Model\Entity\LoginOtp get($primaryKey, $options = [])
 * @method \Admiral\Admiral\Model\Entity\LoginOtp newEntity($data = null, array $options = [])
 * @method \Admiral\Admiral\Model\Entity\LoginOtp[] newEntities(array $data, array $options = [])
 * @method \Admiral\Admiral\Model\Entity\LoginOtp|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Admiral\Admiral\Model\Entity\LoginOtp saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Admiral\Admiral\Model\Entity\LoginOtp patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Admiral\Admiral\Model\Entity\LoginOtp[] patchEntities($entities, array $data, array $options = [])
 * @method \Admiral\Admiral\Model\Entity\LoginOtp findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class LoginOtpTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('login_otp');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
            'className' => 'Admiral/Admiral.Users'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('code')
            ->maxLength('code', 255)
            ->requirePresence('code', 'create')
            ->notEmptyString('code');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }
}
