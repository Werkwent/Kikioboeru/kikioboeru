<?php
namespace Admiral\Admiral\Command;

use Cake\Filesystem\File;
use Cake\Console\Arguments;
use Cake\Console\Command;
use Cake\Console\ConsoleIo;
use Symfony\Component\Yaml\Yaml;

class UsersDefaultCommand extends Command {
  private ConsoleIo $io;
  private $roles;
  private $lockfile = CONFIG . 'default-users.lock';

  public function initialize(): void {
    $this->loadModel('Admiral/Admiral.Users');
    $this->loadModel('Admiral/Admiral.Roles');
  }

  public function execute(Arguments $args, ConsoleIo $io) {
    $this->io = $io;

    // Make sure a lockfile doesn't exist
    // Exit if it does
    $lockfile = new File($this->lockfile, false);
    if($lockfile->exists()) {
      $this->io->out('Lockfile found at "' . $this->lockfile . '"!' . PHP_EOL . 'Skipping user creation!');
      return;
    }

    // Load our YAML file
    // Then parse it
    // Throw an error if it wasn't found
    $this->io->out('Loading YAML...');
    $yaml = new File(CONFIG . 'default-users.yaml', false);
    if(!$yaml->exists()) throw new \Exception('YAML as "' . CONFIG . '/default-users.yaml" could not be found!');
    $users = Yaml::parse($yaml->read());
    
    // Loop over each bundle to create
    foreach($users as $user) {
      // Make sure the user doesn't already exist
      $entity = $this->Users->find()
        ->where([
          'OR' => [
            'username' => $user['username'],
            'email' => $user['email'],
          ],
        ])
        ->first();
      if($entity) {
        $this->io->out('User "' . $user['username'] . '" ("'.$user['email'].'") already exists!');
        continue;
      }

      // Create a new user entity
      $this->io->out('Creating user "' . $user['username'] . '"');
      $entity = $this->Users->newEntity([
        'username' => $user['username'],
        'email' => $user['email'],
        'users_detail' => [
          'firstname' => $user['firstname'],
          'lastname' => $user['lastname'],
        ],
      ]);

      // Get information for each of the selected role
      // Add them to the roles for the user
      $entity->roles = $this->findRoles($user['roles']);

      // Store the user in the database
      if($this->Users->save($entity)) {
        $this->io->out('User "' . $user['username'] . '" has been created!');
      } else {
        $this->io->out('Could not create user "' . $user['username'] . '"!');
      }
    }
    $this->io->out('All new users have been created!');
    $this->io->out('Creating lockfile at "' . $this->lockfile . '"...');
    $lockfile = new File($this->lockfile, true);
    $this->io->out('Lockfile created!');
  }

  public function findRoles(array $roles): array {
    $returnRoles = [];

    foreach($roles as $role) {
      // Check if we already found this role in memory
      // If so, get it from memory and add it to the return roles
      // Then, continue to the next role
      if(!empty($this->roles[$role]))  {
        $returnRoles[] = $this->roles[$role];
        continue;
      }

      // Find the role
      $entity = $this->Roles->findByName($role)->first();
      
      // Check if a role was found
      // If not, skip this role
      if(!$entity) continue;

      // Store this role in memory for later lookups
      $this->roles[$role] = $entity;

      // Add role to our return roles
      $returnRoles[] = $entity;
    }

    return $returnRoles;
  }
}