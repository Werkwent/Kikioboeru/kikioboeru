<?php
  namespace Admiral\Admiral\Form;

  use Cake\Form\Form;
  use Cake\Form\Schema;
  use Cake\Validation\Validator;

  class LoginForm extends Form {
    protected function _buildSchema(Schema $schema) {
      return $schema
        ->addField('username', ['type' => 'string'])
        ->addField('password', ['type' => 'string']);
    }

    protected function _buildValidator(Validator $validator) {
      $validator
      // Rules for the Username field
      ->requirePresence('username')
      ->notEmpty('username', 'Please enter your username')
      // Rules for the Password field
      ->requirePresence('password')
      ->notEmpty('password', 'Please enter your password');
      return $validator;
    }
  }
