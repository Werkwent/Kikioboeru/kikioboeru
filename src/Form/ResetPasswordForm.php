<?php
  namespace Admiral\Admiral\Form;

  use Cake\Form\Form;
  use Cake\Form\Schema;
  use Cake\Validation\Validator;

  class ResetPasswordForm extends Form {
    protected function _buildSchema(Schema $schema) {
      return $schema
        ->addField('token', ['type' => 'string'])
        ->addField('new_password', ['type' => 'string'])
        ->addField('new_password_conf', ['type' => 'string']);
    }

    protected function _buildValidator(Validator $validator) {
      $validator
      // Rules for the token field
      ->requirePresence('token')
      ->notEmpty('token', 'Token cannot be left empty')
      // Rules for the new password field
      ->requirePresence('new_password')
      ->notEmpty('new_password', 'Please enter your new password')
      // Rules for the new Password confirmation field
      ->requirePresence('new_password_conf')
      ->notEmpty('new_password_conf', 'Please confirm your new password')
      // Check wether the two entered passwords are the same
      ->add('new_password_conf', [
        'compare' => [
          'rule' => ['compareWith', 'new_password'],
          'message' => 'New password and confirmation do not match!'
        ]
      ]);
      return $validator;
    }
  }
