<?php
  namespace Admiral\Admiral\Form;

  use Cake\Form\Form;
  use Cake\Form\Schema;
  use Cake\Validation\Validator;

  class ForgotPasswordForm extends Form {
    protected function _buildSchema(Schema $schema) {
      return $schema
        ->addField('email', ['type' => 'string']);
    }

    protected function _buildValidator(Validator $validator) {
      $validator
      // Rules for the Email field
      ->requirePresence('email')
      ->notEmpty('email', 'Please enter your email');
      return $validator;
    }
  }
