<?php
  namespace Admiral\Admiral\Form;

  use Cake\Form\Form;
  use Cake\Form\Schema;
  use Cake\Validation\Validator;

  class RegisterForm extends Form {
    protected function _buildSchema(Schema $schema) {
      return $schema
        ->addField('username', ['type' => 'string'])
        ->addField('email', ['type' => 'string'])
        ->addField('password', ['type' => 'string'])
        ->addField('password_conf', ['type' => 'string']);
    }

    protected function _buildValidator(Validator $validator) {
      $validator
      // Rules for the Username field
      ->requirePresence('username')
      ->notEmpty('username', 'Please enter a username')
      // Rules for the Email field
      ->add('email', 'format', [
        'rule' => 'email',
        'message' => 'Please enter a valid email',
      ])
      // Rules for the Password field
      ->requirePresence('password')
      ->notEmpty('password', 'Please enter a password')
      // Rules for the Password confirmation field
      ->requirePresence('password_conf')
      ->notEmpty('password_conf', 'Please confirm your password')
      // Check wether the two entered passwords are the same
      ->add('password_conf', [
        'compare' => [
          'rule' => ['compareWith', 'password'],
          'message' => 'Password and confirmation do not match!'
        ]
      ]);
      return $validator;
    }
  }
