<?php
  namespace Admiral\Admiral\Form;

  use Cake\Form\Form;
  use Cake\Form\Schema;
  use Cake\Validation\Validator;

  class UserDetailsForm extends Form {
    protected function _buildSchema(Schema $schema) {
      return $schema
        ->addField('firstname', ['type' => 'string'])
        ->addField('lastname', ['type' => 'string']);
    }

    protected function _buildValidator(Validator $validator) {
      $validator
      // Rules for the firstname field
      ->requirePresence('firstname')
      ->notEmpty('firstname', 'Please enter your first name')
      // Rules for the lastname field
      ->requirePresence('lastname')
      ->notEmpty('lastname', 'Please enter your last name');
      return $validator;
    }

    protected function _execute(array $data) {
      return true;
    }

    public function setErrors($errors){
      $this->_errors = $errors;
    }
  }
