<?php
namespace Admiral\Admiral\Test\TestCase\Model\Table;

use Admiral\Admiral\Model\Table\WebauthntokensTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * Admiral\Admiral\Model\Table\WebauthntokensTable Test Case
 */
class WebauthntokensTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \Admiral\Admiral\Model\Table\WebauthntokensTable
     */
    public $Webauthntokens;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.Admiral/Admiral.Webauthntokens',
        'plugin.Admiral/Admiral.Users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Webauthntokens') ? [] : ['className' => WebauthntokensTable::class];
        $this->Webauthntokens = TableRegistry::getTableLocator()->get('Webauthntokens', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Webauthntokens);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
