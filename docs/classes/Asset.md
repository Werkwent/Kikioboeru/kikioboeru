# Asset
###### `class` Admiral\Admiral\Asset
The Asset class is used to add scripts for your plugin to Admiral's dashboard.  
This is useful for when your plugin uses some custom javascript.  

## Usage
Adding assets can be done either by:
- Registering by path
- Inline

### Registering scripts by path
Registering scripts by path is done in the following way, preferably, this is done in the `bootstrap.php` of your plugin:

```php
use Admiral\Admiral\Asset;

Asset::script('Vendor/Plugin.js/myscript.js');
```

### Inline scripts
Adding a script in-line can be done like this, preferably, this is done in the view template of your plugin:

```php
<?php \Admiral\Admiral\Asset::scriptStart(); ?>
  <script>
    console.log('Hello Admiral!');
  </script>
<?php \Admiral\Admiral\Asset::scriptEnd()l ?>
```