# User
###### `class` Admiral\Admiral\User
The User class has been made to easily get a user.  
Is is mostly a variation for `$this->Auth->user()` made for when not using a controller, however, it can be used inside of them just fine.

## Usage
```php
use Admiral\Admiral\User;

$user = User::get();
```