# Menu
###### `class` Admiral\Admiral\Menu
The Menu-class is used for handling the entire sidebar menu in the dashboard.  
It allows you to add a new items and update them if need be.  

## Adding a menu item
Adding a menu item can be done fairly simply by calling the `add()` method.  
The first parameter should be the name of the menu (eg. `main_menu`).  
The second one should be an array of options that can be set to whatever you need.
```php
Menu::add('my_menu', [
  'name' => 'my_item',
  'label' => 'My Item',
  'icon' => 'fas fa-tachometer-alt',
  'weight' => 1,
  'url' => [
    'plugin'=> 'MyCompany/MyPlugin',
    'controller' => 'MyController',
    'action'=> 'index'
  ]
]);
```

## Adding permissions to menu items
Adding permissions to a menu item prevents a user from seeing items the user has no permission for.  
Imagine clicking on a link just to see you shouldn't be there, not nice ain't it?  

Adding a permission can be done by adding the permission key to the menu item.
```php
Menu::add('my_menu', [
  // ... Your menu options
  'permissions' => [
    'my_first_permission',
    'my_second_permission'
  ]
])
```

## Menu options
| Option      | type    | Required? | Description                              |
|-------------|---------|-----------|------------------------------------------|
| name        | String  | Yes       | The internal name of the item            |
| label       | String  | Yes       | The display name of the item             |
| icon        | String  | No        | The icon class for the item              |
| weight      | Integer | No        | The weight for the item                  |
| url         | array   | No        | The CakePHP route for the item           |
| children    | array   | No        | Array of child items                     |
| permissions | array   | No        | Array of permissions needed for the item |