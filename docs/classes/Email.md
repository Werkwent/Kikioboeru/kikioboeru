# Email
###### `class` Admiral\Admiral\Email
The Email class can be used to easily send mails through your preferred source.  
It is meant as a simple to use proxy for the `\Cake\Mail\Mailer` class.

## Usage
```php
use Admiral\Admiral\Email;

$email = new Email();
$email->set('to', 'example@admiral.cms');
$email->set('subject', 'Hello from Admiral!');
$email->set('template', 'email-template');
$email->set('format', 'html'); // Alternatively, use "text"
$email->set('viewVars', [
  'key' => 'value',
]);

try {
  $email->send();
} catch(\Exception $e) {
  // ... Handle exceptions
}
```

### Templates
When sending emails, you can use templates to build the layouts and content of your email and use the `viewVars` for dynamic things (like usernames).    
By default, CakePHP will try to use the template found in your project's `/src/Template/Plugin/Admiral/Admiral/Email/<format>/` and if this doesn't exist, use the default templates provided by Admiral.  
Currently, these are the templates used by Admiral:
- login_code