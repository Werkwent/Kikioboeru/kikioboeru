# Settings
###### `class` Admiral\Admiral\Settings
`Settings` allows you to easily add, set and get settings.  
It is useful for when you want to register settings to the settings page and use them later in your app or plugin.  

> **Note**: It is currently not possible to add a setting through this class.  
> We are aware of this and will look into it in the near future!

## Registering a setting
Registering a setting can be done by calling the `add()` method.
It expects an array for each array in the section.
```php
Settings::add([
  'label' => 'My Plugin\'s Settings',
  'name' => 'my-plugins-settings',
  'options' => [
    [
      [
        'type'=>'text',
        'name'=>'my_first_setting',
        'value'=>Settings::getOptionValue('my_first_setting'),
        'label'=>'My First Setting',
        'placeholder'=>'A regular text input'
      ],
      [
        'type'=>'text',
        'name'=>'my_password',
        'value'=>Settings::getOptionValue('my_password'),
        'label'=>'My Password',
        'placeholder'=>'A Password input'
      ],
    ]
  ]
]);
```

The parent array expects atleast the following values to be present:
| Value   | Description                              |
|---------|------------------------------------------|
| label   | The "human-readable" label               |
| name    | The internal name of the setting         |
| options | Array containing the settings themselves |

## Storing a value
Storing a value can be done by calling the `saveOption()` value.  
It expects the first parameter to be the name of the setting you try to modify and the second to be the value.  
```php
Settings::saveOption('my_setting','the value');
```

## Retrieving a value
Getting the value of a setting can be done by calling the `getOptionValue()` method.  
It only expects one parameter being the name of the setting.  
```php
$value = Settings::getOptionValue('my_setting');
```