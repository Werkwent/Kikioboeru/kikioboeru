# Permission
###### `class` Admiral\Admiral\Permission
The permission class allows you to register permissions and to get the label associated with a permission, as well as check whether the user has a certain permission or not.  

## Checking a permission
Checking a permissions can be done by calling the `check()` method.  
```php
$hasPermission = Permission::check('my_permission');
```

The method will return one of the following values:
| Value | Description           |
|-------|-----------------------|
| -1    | User is not logged in |
| 0     | User lacks permission |
| 1     | User has permission   |

It is also possible to check for a specific value immediately by passing the value to the method.  
Doing this will return `true` or `false` depending on the condition.
```php
$hasPermission = Permission::check('my_permission', 1);
```