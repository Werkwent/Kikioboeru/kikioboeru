# User Avatars
As of Admiral `2.1.0`, Admiral supports user avatars.  
Currently, user avatars are stored in `WWW_ROOT . DS . $pathPrefix . 'user-avatars' . DS . $username . '.webp'`.  
Admiral will take care of ensuring an avatar was found and falling back on defaults if not in the following order:
1. User specific avatar.
2. App default avatar.
3. Admiral's default avatar.

Please do note that *only* images in the `webp` format are supported!

## Setting An User Avatar
Setting an avatar is done through a GraphQL mutation:
```gql
mutation($data: String) {
  updateAvatar(data: $data) {
    success
    message
  }
}
```

It expects the `data` variable to be a base64encoded dataurl of the image.  
This can be done in JS using `FileReader.readAsDataURL`.  
***note:*** Images *must* be under 5MB.

## Getting An User Avatar
Once an avatar has been set, it can be retrieved from a `UserEntity` using `$user->avatar()`.  
Doing so yields an absolute path (with protocol and domain) to the avatar.  
```php
// Obtain the user from the database
$user = $this->Users->findById(1)->first();

// Obtain the avatar
$avatar = $user->avatar();
var_dump($avatar); // string(57) "https://localhost/admiral/admiral/img/default-avatar.webp"
```