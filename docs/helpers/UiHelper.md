# UiHelper
The UiHelper is made to keep stylings and functionality more consistent between plugins, the specific project and Admiral itself.  
It contains a variety of preset elements like cards, tables and a text-editor (TinyMCE).  

## Preperation
Before you can use the UiHelper, you'll need to either load it up in your `View`:
```php
$this->loadHelper('Admiral/Admiral.Ui');
```

or change the View to the provided by Admiral:
```
$this->viewBuilder()->setClassName('Admiral/Admiral.App');
```

More documentation to follow...