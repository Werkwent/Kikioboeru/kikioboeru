# Permissions List
Below is a comprehensive list of permissions used in this package.

| Node | Description |
|------|-------------|
| admiral.admiral.cms.access | Allow the user to access the CMS |
| admiral.admiral.settings.edit | Allow the user to edit settings |
| admiral.admiral.media.view | Allow the user to view media |
| admiral.admiral.media.add | Allow the user to upload new media |
| admiral.admiral.media.edit | Allow the user to edit media |
| admiral.admiral.media.delete | Allow the user to delete media|
| admiral.admiral.users.view | Allow the user to view profiles of other users |
| admiral.admiral.users.add | Allow the user to create new users |
| admiral.admiral.users.edit | Allow the user to edit the profiles of other users |
| admiral.admiral.users.delete | Allow the user to delete users |