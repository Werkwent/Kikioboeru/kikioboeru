# Quick Start
Here is a quickstart guide for implementing Admiral in your project.
It consists of the following steps:
- [Installing the package](#installing-package)
- [Loading into CakePHP](#loading-into-cakephp)
- [Running migrations](#running-migrations)
- [Setting up Roles and Permissions](#getting-roles-and-permissions-setup)
- [Creating your first user](creating-your-first-user)
- [Enabling the AuthComponent](#enable-authcomponent)
- [Setting up Emails](#setup-email)
- [Setting up a default Avatar](#setting-default-avatar)

## Dependencies
- CakePHP `3.x` (CakePHP 4.x currently unsupported)
- PHP `7.4.x` (PHP `8.x` currently unsupported)
- PHP Imagick Extension

## Installing
Installing Admiral takes a few steps to get to work properly.
In this section, we'll cover those steps so you can get started as quickly as possible!

### Installing Package
```
composer require admiral/admiral
```

### Loading into CakePHP
In order to get Admiral to work, we'll need to load it into CakePHP by adding the following to our `src/Application.php`:
```php
// src/Application.php
public function bootstrap() {
  parent::bootstrap();

  $this->addPlugin('Admiral/Admiral',['bootstrap' => true]);
  $this->addPlugin('Admiral/GraphQL',['bootstrap' => true]);
  $this->addPlugin('FinlayDaG33k/FileUpload',['bootstrap' => true]);
}
```

### Running Migrations
Next, you'll need to run the migrations.
```
bin/cake migrations migrate -p Admiral/Admiral
```

### Getting Roles and Permissions setup
Next, you'll need to get the permissions setup.
To do so, create a file `config/permissions.yaml` and paste the following contents in it:
```yaml
Member:
  alias: 'Member'
  default: true
  permissions:
Administrator:
  alias: 'Administrator'
  default: false
  permissions:
    - admiral.admiral.cms.access
```

This will do the following:
- Create two roles ("Member" and "Administrator").
- Set their aliases (for display in the front-end).
- Set "Members" as the default role.
- Allow users of the group "Administrator" to access the CMS.

Feel free to customize the roles and their permissions to your needs.
You can find a list of permissions [here](permissions-list.md).
Plugins for use with Admiral all have their own permission nodes, see their respective documentations for more information.

Finally, you need to load the permissions, this is done by adding the following to your `bootstrap.php`:
```php
use Admiral\Admiral\Permission;

Permission::load();
```

If you use some form of CI/CD (eg. GitLab CI/CD, TravisCI etc.), you may want to only load permissions when not running in a CLI:
```php
use Admiral\Admiral\Permission;

if(!$isCli) Permission::load();
```

### Creating your first user
Of course, we need to create a user to start using Admiral!
This can be done by creating the `config/default-users.yaml` file:
```yaml
- username: "johndoe"
  firstname: "John"
  lastname: "Doe"
  email: "john.doe@example.com"
  roles:
    - Administrator
    - Member
```

Then run the following command:
```
bin/cake users default
```

This will do the following:
- Create a new user with `johndoe` as username and `john.doe@example.com` as email.
- Set the user's firstname to `John` and lastname to `Doe`.
- Add the user to the roles `Administrator` and `Member` (as specified in [Getting Roles and Permissions setup](#getting-roles-and-permissions-setup))
- Create a lockfile as `config/default-users.lock` to prevent this command from accidentally being re-ran (useful for CI/CD deployed applications)

### Enable AuthComponent
Currently, Admiral relies on CakePHP's built-in AuthComponent.
In order to load this, add the following to your project's `AppController.php`.
Please refer to the [CakePHP Cookbook](https://book.cakephp.org/3/en/controllers/components/authentication.html) fore more information on this component.
```php
// src/Controller/AppController.php
$this->loadComponent('Auth');
```

### Setup Email
In order to authenticate with Admiral through email (and later send other mails with it), you'll need to setup the Email variables found in your `config/app.php`.
To do so, add the following line to your `.env` (implying SMTP):
```
export EMAIL_TRANSPORT_DEFAULT_URL="smtp://username@myapp.tld:password@smtp.host:587"
```

**NOTE**: You can enable TLS by adding `?tls=true` behind the DSN string.
**NOTE**: You may need to change the port based on what your provider requires.

Next, you may want to update the `from` attribute in your config.
To do so, add the following to your `config/app.php`:
```php
return [
  // ... Some other config
  'EmailTransport' => [
    'default' => [
      // ... Some email config
      'from' => [
        'mail' => env('SMTP_DEFAULT_FROM', null),
        'name' => env('SMTP_DEFAULT_NAME', null),
      ],
    ],
  ],
];
```

Then, add the following lines to your `.env`:
```
export SMTP_DEFAULT_FROM="noreply@myapp.tld"
export SMTP_DEFAULT_NAME="My App"
```

### Setting Default Avatar
By default, Admiral ships with it's own default avatar, however, if you'd like, you can set your own really simply.  
Just add a `default-avatar.webp` to your app's `img` folder and that's it.

Admiral will return an avatar in the following order (returning on first found):
1. User specific avatar.
2. App default avatar.
3. Admiral's default avatar.

See [User Avatars](user-avatars.md) for more information.
